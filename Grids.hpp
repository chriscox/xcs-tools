//
//  Grids.hpp
//      Define grid parameters structure and function calls.
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#ifndef grids_h
#define grids_h

#include <cstdint>
#include <string>
#include <vector>
#include "json.hpp"
#include "xcs_document.hpp"

/******************************************************************************/

using namespace std;
namespace fs = std::filesystem;

using json = nlohmann::json;
//using json = nlohmann::ordered_json;

/******************************************************************************/
/******************************************************************************/

// The maximum potential value on D1 is 10 passes * (300/166 lpc) * 100power / 1 = 1807
// Assume higher limits for another theoretical device: 50 * 1000 * 100 / 1 = 5e6, plus some slop and call it 1e7
// Scratch paper starts to burn around 60*200/166/200= 0.36 and 10*200/166/30 = 0.4
//      and burns completely through around 30*200/166/20 = 1.8  and 100*200/166/160 = 0.8
const double defaultExposureLimit = 1e7; // no real limit, burn it all

/******************************************************************************/

struct gridDefinition {

    // default to invalid settings and empty lists
    gridDefinition() : boxCutType(xcsFillEngraving), boxSize(0),
                    boxSpacing(0), groupSpacing(0),
                    labelCutType(xcsFillEngraving), labelSize(0), labelPower(0),
                    labelSpeed(0), labelPasses(0), labelDensity(0),
                    exposureLimit( defaultExposureLimit ) {}


public:
    xcsCutType boxCutType;  //
    vector<int> powers;     // 0 - 100
    vector<int> speeds;     // 1 to 500 ? mm/s
    vector<int> passes;     // 1-10
    vector<int> densities;  // lines/cm
    double boxSize;         // in mm
    double boxSpacing;      // in mm
    double groupSpacing;    // in mm
    
    double exposureLimit;   // exposure = (density/166)*passes*power/speed
    
    xcsCutType labelCutType; //
    double labelSize;        // in points
    int labelPower;          // 1-100
    int labelSpeed;          // mm/s
    int labelPasses;         // 1-10
    int labelDensity;        // lines/cm

public:

    // Sigh. C++20 can't generate an automatic <=> operator for this.
    // Because xcsCutType has no order?
    // Or because vectors have no order?
    bool operator==(const gridDefinition& b) const
    {
        return boxCutType == b.boxCutType
            && powers == b.powers
            && speeds == b.speeds
            && passes == b.passes
            && densities == b.densities
            && boxSize == b.boxSize
            && boxSpacing == b.boxSpacing
            && groupSpacing == b.groupSpacing
            && labelCutType == b.labelCutType
            && labelSize == b.labelSize
            && labelPower == b.labelPower
            && labelSpeed == b.labelSpeed
            && labelPasses == b.labelPasses
            && labelDensity == b.labelDensity
            && exposureLimit == b.exposureLimit;
    }
    
    bool isValid(bool printExplanation = false) const;
};

/******************************************************************************/

void to_json( json &j, const gridDefinition &g );

void from_json( const json &j, gridDefinition &g );

/******************************************************************************/

void CreateEngraveGrid( const string &outfilename, const gridDefinition &def );

/******************************************************************************/

#endif /* grids_h */
