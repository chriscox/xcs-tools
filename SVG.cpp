//
//  SVG.cpp
//      Write SVG file from XCS file contents
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#include <cstdint>
#include <string>
#include <iostream>
#include "xcs_document.hpp"

/******************************************************************************/

using namespace std;

/******************************************************************************/
/******************************************************************************/

// NOTE - the viewBox and background are always interpreted as points for some reason, can't use mm for scale modification
void WriteSVGHeader( std::ostream &out, point2D topLeft, point2D bottomRight )
{
	out << "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	out << "<svg version=\"1.1\" id=\"Layer_1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"\n";
	out << "\tx=\"0\" y=\"0\" viewBox=\"" << (topLeft.x*mm2point) << " " << (topLeft.y*mm2point)
            << " " << (bottomRight.x*mm2point) << " " << (bottomRight.y*mm2point) << "\" ";
    out << "style=\"enable-background:new " << (topLeft.x*mm2point) << " " << (topLeft.y*mm2point)
            << " " << (bottomRight.x*mm2point) << " " << (bottomRight.y*mm2point) << ";\" xml:space=\"preserve\">\n";
}

/******************************************************************************/

void WriteSVGFooter( std::ostream &out )
{
	out << "</svg>\n";
}

/******************************************************************************/

// On "reflection":
// Someone really didn't understand what skew meant...
//          or how a hack like this is going to bite them in the a** later.
// Just 2 integer flags could have indicated reflection, or negative signs on the scale.

// Could they be using a broken transform decomposition that doesn't know about reflection/mirroring? Probably tries to keep scale positive.

// rotated and reflected changes localskew and skew, no longer matching

// Reflected Horizontal + rotate 180 == Reflected Vertical
// Reflected Vertical + rotate 180 == Reflected Horizontal
// Reflected Horizontal + Reflected Vertical == rotate 180

// algorithm guess: ignore sign on PI (because +-180 is still 180), ignore localSkew, just use PI flag to reflect vertical (and recenter), then rotate by specified angle about center
// probably need to pass in full object data to get all the coordinates needed


// reflectedVertical = angle 0, skew PI,0, localSkew PI,0      == scale(1,-1)
// reflectedHorizontal = angle 180, skew PI,0, localSkew PI,0   == scale(-1,1)
// straight = angle 0, skew 0,0, localSkew 0,0      == scale(1,1)
// rotated 180 = angle 180, skew 0,0, localSkew 0,0 == scale(1,1)

// 30 degrees = 0.523598775 radians
// reflectedVertical rot 30 = angle 30, skew PI,0, localSkew":{"x":2.6179938779914944,"y":0.5235987755982988}
// reflctedHorizontal rot 30 = angle -150, skew -PI,0, localSkew":{"x":-0.5235987755982987,"y":-2.6179938779914944}
// straight rot 30 = angle 30, skew 0,0, localSkew 0,0

// -30 degrees = -0.52359775 radians
// reflectedVertical rot -30 = angle -30, skew -PI,0, localSkew":{"x":-2.6179938779914944,"y":-0.523598775598299}
// reflctedHorizontal rot -30 = angle 150, skew PI,0, localSkew":{"x":0.5235987755982987,"y":2.6179938779914944}
// straight rot -30 = angle -30, skew 0,0, localSkew 0,0

// 90 degrees = 1.570793627 radians
// reflectedVertical rot 90 = angle 90, skew PI,0, localSkew":{"x":1.5707963267948963,"y":1.5707963267948968}
// reflctedHorizontal rot 90 = angle -90, skew -PI,0, localSkew":{"x":-1.5707963267948966,"y":-1.5707963267948966}
// straight rot 90 = angle 90, skew 0,0, localSkew 0,0

// -90 degrees = -1.570793627 radians
// reflectedVertical rot -90 = angle -90, skew -PI,0, localSkew":{"x":-1.5707963267948968,"y":-1.5707963267948963}
// reflctedHorizontal rot -90 = angle 90, skew PI,0, localSkew":{"x":1.5707963267948966,"y":1.5707963267948966}
// straight rot -90 = angle -90, skew 0,0, localSkew 0,0

// 180 degrees == PI radians
// reflectedVertical rot 180 = angle -180, skew -PI,0, localSkew":{"x":-4.440892098500626e-16,"y":-3.141592653589793}
// reflctedHorizontal rot 180 = angle 0, skew -PI,0, "localSkew":{"x":-3.141592653589793,"y":-3.8741581118524966e-17}
// straight rot 180 = angle -180, skew 0,0, localSkew 0,0

// 120 degrees = 2.094395102 radians
// reflectedVertical rot 120 = angle 120, skew PI,0, "localSkew":{"x":1.0471975511965979,"y":2.0943951023931957}
// reflctedHorizontal rot 120 = angle -60, skew -PI,0, "localSkew":{"x":-2.094395102393195,"y":-1.047197551196598}
// straight rot 120 = angle 120, skew 0,0, localSkew 0,0

// -120 degrees = -2.094395102 radians
// reflectedVertical rot -120 = angle -120, skew -PI,0, "localSkew":{"x":-1.0471975511965979,"y":-2.0943951023931957}
// reflctedHorizontal rot -120 = angle 60, skew PI,0, "localSkew":{"x":2.094395102393195,"y":1.047197551196598}
// straight rot -120 = angle -120, skew 0,0, localSkew 0,0


// offsetX was wrong choice for translation, now just outline rect is wrong
/*
<polygon points="468.562,506.607 739.973,506.607 739.973,636.227 468.562,636.227 " transform=" scale(1 -1) translate(0 -2145.06) scale(0.989664 0.989664)" fill="none" stroke="red" stroke-width="2" />


*/


// transform coords don't seem to like mm modifiers, always work in points
// Paths have to scale points->mm
// other objects use relative scale
void AddSVGTransform( std::ostream &out, object_data &obj, double angle, double x=0.0, double y=0.0, point2D scale = point2D(1,1), bool needTranslation = false )
{
    // null transform? just return
    if ( (angle == 0.0) && (scale == point2D(1,1)) && (obj.skew == point2D(0,0))
        && (!needTranslation || ((x== 0.0) && (y == 0.0))) )
        return;
    
    out << " transform=\"";
    
    if (obj.skew != point2D(0,0))
        {
        // SVG clips skew angles to 0-90, so can't use the literal skews from the file
        out << " scale(1 -1)";  // flip vertical, allow rotation to handle 180 horizontal flip
        out << " translate(0 " << -((2*obj.offsetY)*mm2point) << ")";     // translate back into place, works for text
        }
    
    if (angle != 0.0)
        out << " rotate(" << angle << ", " << (x*mm2point) << ", " << (y*mm2point) << ")";
    
    if (needTranslation && x != 0.0 && y != 0.0)
        {
        //out << " translate(" << (obj.offsetX*mm2point) << " " << (obj.offsetY*mm2point) << ")";
        out << " translate(" << (x*mm2point) << " " << (y*mm2point) << ")";
        }
    
    if (scale != point2D(1,1))
        out << " scale(" << scale.x << " " << scale.y << ")";
    
    out << "\"";
}

/******************************************************************************/

// this could be made variable, but just static colors for now
string DefaultFillStroke( bool isFilled )
{
    if (isFilled)
        return string("fill=\"darkorange\" stroke=\"none\"");
    else
        return string("fill=\"none\" stroke=\"indigo\" stroke-width=\"0.2\"");
}

/******************************************************************************/

void WriteSVGCircle( std::ostream &out, object_data &obj  )
{
    // SVG uses coordinates of the center, XCS uses upper left
    double x = obj.x + obj.width/2;
    double y = obj.y + obj.height/2;
    double r = obj.width / 2;
	out << "<circle cx=\"" << x << "mm\" cy=\"" << y << "mm\" r=\""
        << r << "mm\"";
    //AddSVGTransform( out, obj, obj.angle, obj.x, obj.y );    // ???? Does a circle ever get a transform in XCS?
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGEllipse( std::ostream &out, object_data &obj )
{
    if (obj.width == obj.height)
        {
        WriteSVGCircle( out, obj );
        return;
        }
    
    // SVG uses coordinates of the center, XCS uses upper left
    double x = obj.x + obj.width/2;
    double y = obj.y + obj.height/2;
	out << "<ellipse cx=\"" << x << "mm\" cy=\"" << y << "mm\" rx=\""
            << obj.width << "mm\" ry=\"" << obj.height << "mm\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y );
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGLine( std::ostream &out, object_data &obj )
{
    double x2 = obj.x + obj.width;
    double y2 = obj.y + obj.height;
	out << "<line x1=\"" << obj.x << "mm\" y1=\"" << obj.y << "mm\" x2=\""
            << x2 << "mm\" y2=\"" << y2 << "mm\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y ); // yes, lines get transforms from flips
    out << " " << DefaultFillStroke(false) << " />\n";
}

/******************************************************************************/

void WriteSVGRect( std::ostream &out, object_data &obj )
{
	out << "<rect x=\"" << obj.x << "mm\" y=\"" << obj.y << "mm\" width=\""
            << obj.width << "mm\" height=\"" << obj.height
            << "mm\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y );
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGPolyline( std::ostream &out, object_data &obj )
{
	out << "<polyline points=\"";
 
    for ( auto &item : obj.points)
        out << mm2point*item.x << "," << mm2point*item.y << " ";
    
    if (obj.isClosePath)
        out << mm2point*obj.points[0].x << "," << mm2point*obj.points[0].y << " ";
    
    out << "\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y, obj.scale );
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

// May have been used only in beta XCS?
// Apparently offsets center instead of top left - not sure about transforms
void WriteSVGPolygon( std::ostream &out, object_data &obj )
{
	out << "<polygon points=\"";
    
    size_t count = obj.polyPoints.size() >> 1;
    for (size_t i = 0; i < count; ++i)
        {
        double x = obj.polyPoints[2*i+0] + obj.x + obj.width/2;
        double y = obj.polyPoints[2*i+1] + obj.y - obj.height/2;
        out << x*mm2point << "," << y*mm2point << " ";
        }
    
    out << "\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y, obj.scale );
    out << " " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

#if 0
{"id":"e6bff683-61b6-4380-8ece-b8c58ace9316","type":"PATH",
"x":23.040680387126855,"y":97.7097383024288,
"angle":0,
"scale":{"x":0.9999999247594239,"y":0.9999999247594239},
"skew":{"x":0,"y":0},
"pivot":{"x":0,"y":0},
"localSkew":{"x":0,"y":0},
"offsetX":22.040680462367426,"offsetY":96.70973837766938,   // same as graphicXY, not quite the same as XY
"lockRatio":true,
"isClosePath":true,
"zOrder":-8,
"width":50.69999694824219,"height":47.999996388452345,     // same regardless of rotation!
"isFill":false,
"lineColor":16421416,"fillColor":16421416,
"points":[],"dPath":"M1 25 26.3 1l25.4 24h-16v24H17V25z",
"graphicX":22.04068046236742,"graphicY":96.70973837766937   // same as OffsetXY
}

{"id":"da30b339-fe1b-42b9-b34f-208ada1a6a71",
"type":"PATH",
"x":132.31666054136502,"y":96.35973802253392,
"angle":90,
"scale":{"x":0.9999999247594242,"y":0.9999999247594242},
"skew":{"x":0,"y":0},
"pivot":{"x":0,"y":0},
"localSkew":{"x":0,"y":0},
"offsetX":133.31666046612443,"offsetY":95.3597380977745,   // same as graphicXY, not quite the same as XY
"lockRatio":true,
"isClosePath":true,
"zOrder":-7,
"width":50.6999969482422,"height":47.99999638845236,     // same regardless of rotation!
"isFill":false,
"lineColor":16421416,"fillColor":16421416,
"points":[],"dPath":"M1 25 26.3 1l25.4 24h-16v24H17V25z",
"graphicX":133.31666046612446,"graphicY":95.3597380977745   // same as OffsetXY
}

{"id":"c1fb49a2-e01c-40c1-9831-e206a7f47c45",
"type":"PATH",
"x":204.1686311246944,"y":147.0597349707761,
"angle":-90,
"scale":{"x":0.9999999247594242,"y":0.9999999247594242},
"skew":{"x":0,"y":0},
"pivot":{"x":0,"y":0},
"localSkew":{"x":0,"y":0},
"offsetX":203.16863119993496,"offsetY":148.0597348955355,   // same as graphicXY, not quite the same as XY
"lockRatio":true,
"isClosePath":true,
"zOrder":-5,
"width":50.6999969482422,"height":47.99999638845236,     // same regardless of rotation!
"isFill":false,
"lineColor":16421416,"fillColor":16421416,
"points":[],"dPath":"M1 25 26.3 1l25.4 24h-16v24H17V25z",
"graphicX":203.16863119993496,"graphicY":148.0597348955355   // same as OffsetXY
}

{"id":"22fa3b6a-4a8a-4588-b33c-26efe136a5e0",
"type":"PATH",
"x":193.5926443071508,"y":145.70973469088113,
"angle":180,
"scale":{"x":0.9999999247594245,"y":0.9999999247594245},
"skew":{"x":0,"y":0},
"pivot":{"x":0,"y":0},
"localSkew":{"x":0,"y":0},
"offsetX":194.59264423191019,"offsetY":146.70973461564057,   // same as graphicXY, not quite the same as XY
"lockRatio":true,
"isClosePath":true,
"zOrder":-6,
"width":50.699996948242216,"height":47.999996388452374,     // same regardless of rotation!
"isFill":false,
"lineColor":16421416,"fillColor":16421416,
"points":[],"dPath":"M1 25 26.3 1l25.4 24h-16v24H17V25z",
"graphicX":194.59264423191024,"graphicY":146.70973461564057   // same as OffsetXY
}
#endif

/******************************************************************************/

// paths are still a little bit off position -- x and y too great
//      subtract width/height? seems too large
//      off by 2-5mm    ,       2.9mm x, 2.1mm y        subtracting mm2point is closer, but still wrong
//      transform errors?  happens without rotation
//      Is scale wrong, or scale center?  or transform order?
void WriteSVGPath( std::ostream &out, object_data &obj )
{
	out << "<path";

    point2D mmScale =  mm2point * obj.scale;
    AddSVGTransform( out, obj, obj.angle, obj.offsetX, obj.offsetY, mmScale, true ); 

    out << " d=\"" << obj.dPath << "\" " << DefaultFillStroke(obj.isFill) << " />\n";
}

/******************************************************************************/

void WriteSVGImage( std::ostream &out, object_data &obj )
{
	out << "<image x=\"" << obj.x << "mm\" y=\"" << obj.y << "mm\" width=\""
        << obj.width << "mm\" height=\"" << obj.height << "mm\" xlink:href=\""
        << obj.base64data
        << "\"";
    AddSVGTransform( out, obj, obj.angle, obj.x, obj.y );
    out << " />\n";
}

/******************************************************************************/

void WriteSVGText( std::ostream &out, object_data &obj )
{
	out << "<text";
 
    // SVG defines text point as baseline, not top left
    // baseline seems to be stored in offsetY, and height includes all ascenders and descenders
    double y = obj.offsetY;
    double x = obj.x;
    
    // TODO - not perfect, anchor point is still off in some cases
    if (obj.style.align != string())
        {
        // TODO - could create map of input to output strings
        if (obj.style.align == "left" || obj.style.align == "Left")
            out << " text-anchor=\"start\"";
        else if (obj.style.align == "middle" || obj.style.align == "Middle"
                || obj.style.align == "center" || obj.style.align == "Center")
            out << " text-anchor=\"middle\"";
        else if (obj.style.align == "right" || obj.style.align == "Right")
            out << " text-anchor=\"end\"";
        else
            cerr << "WARNING - unknown text alignment " << obj.style.align << " while exporting SVG\n";
        }
    
    out << " x=\"" << x*mm2point << "\" y=\"" << y*mm2point << "\"";
    
    if (obj.style.fontFamily != string())
        out << " font-family=\"" << obj.style.fontFamily << "\"";
    
    // "Regular", "Bold, "Italic", "Bold Italic", "Light", "Oblique", "Light Oblique", "Bold Oblique", etc.
    if (obj.style.fontSubfamily != string())
        {
        // TODO - could create map of input to output strings
        if (obj.style.fontSubfamily == "Regular" || obj.style.fontSubfamily == "Normal")
            out << " font-weight=\"normal\"";
        else if (obj.style.fontSubfamily == "Bold")
            out << " font-weight=\"bold\"";
        else if (obj.style.fontSubfamily == "Italic")
            out << " font-style=\"italic\"";
        else if (obj.style.fontSubfamily == "Oblique")
            out << " font-style=\"oblique\"";
        else if (obj.style.fontSubfamily == "Light")
            out << " font-weight=\"lighter\"";
        else if (obj.style.fontSubfamily == "Light Oblique")
            out << " font-weight=\"lighter\"" << " font-style=\"oblique\"";
        else if (obj.style.fontSubfamily == "Bold Oblique")
            out << " font-weight=\"bold\"" << " font-style=\"oblique\"";
        else if (obj.style.fontSubfamily == "Light Italic")
            out << " font-weight=\"lighter\"" << " font-style=\"italic\"";
        else if (obj.style.fontSubfamily == "Bold Italic")
            out << " font-weight=\"bold\"" << " font-style=\"italic\"";
        else
            cerr << "WARNING - unknown font sub family " << obj.style.fontSubfamily << " while exporting SVG\n";
        }
    
    out << " font-size=\"" << obj.style.fontSize << "pt\"";

    AddSVGTransform( out, obj, obj.angle, x, obj.offsetY );
    
    out << ">" << obj.text << "</text>\n";
}
/******************************************************************************/

// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform

// TODO: pages/layers for each canvas
// Hmm, apparently many implementations ignore pages, or only import the first.
// https://www.w3.org/TR/2004/WD-SVG12-20041027/multipage.html

void ExportSVGFile( std::ostream &out, XCSDocument &doc )
{
    point2D topLeft;
    point2D bottomRight;
    
    FindDocumentBounds( doc, topLeft, bottomRight );
    WriteSVGHeader( out, topLeft, bottomRight );

    for ( auto& value: doc.canvas)
        {
        // value.title;
        
        for ( auto& [key2, value2] : value.objects )
            {
            if (value2.type == "RECT")
                {
                WriteSVGRect( out, value2 );
                }
            else if (value2.type == "CIRCLE")
                {
                WriteSVGEllipse( out, value2 );
                }
            else if (value2.type == "LINE")
                {
                WriteSVGLine( out, value2 );
                }
            else if (value2.type == "PATH")
                {
                WriteSVGPath( out, value2 );
                }
            else if (value2.type == "PEN")
                {
                WriteSVGPolyline( out, value2 );
                }
            else if (value2.type == "POLYGON")
                {
                // this may have been used only in beta XCS, but appears in some Etsy files!
                WriteSVGPolygon( out, value2 );
                }
            else if (value2.type == "BITMAP")
                {
                WriteSVGImage( out, value2 );
                }
            else if (value2.type == "TEXT")
                {
                WriteSVGText( out, value2 );
                }
            else
                {
                cerr << "WARNING - unknown XCS object type " << value2.type << " while exporting SVG.\n";
                }
            }   // objects on canvas

        }   // canvases

    WriteSVGFooter(out);
}

/******************************************************************************/
/******************************************************************************/
