//
//  xcs_document.hpp
//  xcs_tools
//
//  Created by Chris Cox on 11/20/22.
//  Copyright 2022-2023, by Chris Cox.
//

/*
namespace?

XCS
xcs
xcs_tools

*/

#ifndef xcs_document_h
#define xcs_document_h

#include <string>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include "json.hpp"
#if __cplusplus >= 202000UL
#include <compare>
#endif

/******************************************************************************/

using namespace std;

using json = nlohmann::json;
//using json = nlohmann::ordered_json;

/******************************************************************************/

// we won't be doing anything fancy, and they only need to be document unique
typedef string GUID;

/******************************************************************************/

const double inch = 25.4;   // 25.4 millimeters per inch, international standard

const double point = inch / 72.0; // 72 points per inch, DTP and W3C standard

const double mm2point = 72.0 / inch;        // 2.834645669 (Shows up severeal places)

/******************************************************************************/

struct point2D {
    point2D(double xx, double yy) : x(xx), y(yy) {}
    point2D() : x(0.0), y(0.0) {}
    double x, y;

public:

#if __cplusplus >= 202000UL
    auto operator<=>(const point2D&) const = default;
#else
    bool operator==(const point2D& b) const
                { return x == b.x && y == b.y; }
    bool operator!=(const point2D& b) const
                { return x != b.x || y != b.y; }
#endif

};

inline point2D operator+(const point2D& xx, const point2D& yy) {
    return point2D(xx.x + yy.x, xx.y + yy.y);
}

inline point2D operator-(const point2D& xx, const point2D& yy) {
    return point2D(xx.x - yy.x, xx.y - yy.y);
}

inline point2D operator*(const point2D& xx, const point2D& yy) {
    return point2D(xx.x * yy.x, xx.y * yy.y);
}

inline point2D operator/(const point2D& xx, const point2D& yy) {
    return point2D(xx.x / yy.x, xx.y / yy.y);
}

inline point2D operator*(const double s, const point2D& yy) {
    return point2D(s * yy.x, s * yy.y);
}

/******************************************************************************/

struct rect2D {
    rect2D(double xmin, double ymin, double xmax, double ymax) :
            minX(xmin), maxX(xmax), minY(ymin), maxY(ymax) {}
    
    rect2D() : minX(0), maxX(0), minY(0), maxY(0) {}
    
    bool isNull() const { return minX == 0.0 && minY == 0.0 && maxX == 0.0 && maxY == 0.0; }
    
    double minX, maxX, minY, maxY;
};

void to_json( json &j, const rect2D &g );

/******************************************************************************/

struct text_style_struct {

    text_style_struct() : align("left"), fontFamily("Arial"),
                        fontSubfamily("Regular"),
                        fontSize(24), leading(0), letterSpacing(0),
                        curveX(0), curveY(0) {}

    string align;           //  "Left", "Middle", "Right"
    string fontFamily;      // "Arial", etc.
    double fontSize;        // in points
    string fontSubfamily;   // "Regular", "Bold, "Italic", "Bold Italic", could be open ended
    double leading;         // in points
    double letterSpacing;   // in points
    string fontSource;      // string, seems pointless
    
    // used by SVG export, should not be included for equality or hashing
    string cachedSVGStyle;
    
    double curveX;  // 1.4.11
    double curveY;  // 1.4.11

public:
    // NOTE - we can get rid of this once C++20 stabilizes
    //auto operator<=>(const gridDefinition&) const = default;
    bool operator==(const text_style_struct& b) const
    {
        return align == b.align
            && fontFamily == b.fontFamily
            && fontSize == b.fontSize
            && fontSubfamily == b.fontSubfamily
            && leading == b.leading
            && letterSpacing == b.letterSpacing;
    }
};

/******************************************************************************/

struct glyph_data {
    glyph_data() : advanceHeight(0), advanceWidth(0), leftBearing(0), topBearing(0) {}

    string key;     // really a single char
    double advanceHeight;
    double advanceWidth;
    string dPath;
    
    rect2D bbox;        // 1.4.11
    double leftBearing; // 1.4.11
    double topBearing;  // 1.4.11
};

typedef unordered_map<string,glyph_data> glyphDataMap;

/******************************************************************************/

struct font_info {
    font_info() : lineHeight(0), unitsPerEm(0), ascent(0),
            capHeight(0), descent(0), lineGap(0), xHeight(0) {}

    double lineHeight;
    double unitsPerEm;
    
    double ascent;     // 1.4.11
    double capHeight;  // 1.4.11
    double descent;    // 1.4.11
    double lineGap;    // 1.4.11
    double xHeight;    // 1.4.11
};

/******************************************************************************/

struct font_data {
    font_data() : valid_(false) {}

    bool valid_;
    font_info   fontInfo;
    glyphDataMap  glyphData;
};

/******************************************************************************/

// forward definition, because someone got recursive with character glyphs
struct object_data;
typedef vector<object_data> charObjectArray;

struct object_data {

    object_data() : angle(0), height(0), width(0), x(0), y(0), isClosePath(true),
                    isFill(false), lockRatio(true), offsetX(0), offsetY(0),
                    zOrder(0), fillColor(0xFA9228), lineColor(0x9926ff),
                    graphicX(0), graphicY(0), resolution(1), grayValue({0,255}),
                    sharpness(50), scale(1,1), colorInverted(false) {}

    // required values
    GUID id;
    double angle;
    double height;
    double width;
    double x;
    double y;
    bool isClosePath;
    bool isFill;
    bool lockRatio;
    double offsetX;
    double offsetY;
    point2D localSkew;
    point2D pivot;
    point2D skew;
    point2D scale;
    string  type;   // CIRCLE, RECT, LINE, PATH, PEN, TEXT, BITMAP, POLYGON
    int zOrder;     // starts at zero and decrements?  Or all zero.  Can this set burn order?
    
    // optional values
    int fillColor;              // CIRCLE, RECT, LINE, PATH, PEN, POLYGON
    int lineColor;              // CIRCLE, RECT, LINE, PATH, PEN, POLYGON
    
    point2D endPoint;           // LINE only
    
    double graphicX;            // PATH
    double graphicY;            // PATH
    
    vector<point2D> points;     // PATH, empty; PEN, list of Point2D
    // controlPoints ???        // PEN only, always empty struct so far
    vector<double> polyPoints;   // POLYGON, list of doubles
    
    double resolution;          // TEXT only, seems to be 1
    string text;                // TEXT only
    text_style_struct style;    // TEXT only
    
    vector<int>  grayValue;     // BITMAP, 2 item list
    int sharpness;              // BITMAP
    vector<string> filterList;  // BITMAP, may be empty
    string base64data;          // BITMAP, PNG image data, base64 encoded
    bool colorInverted;         // BITMAP, added in 1.2.14
    
    GUID sourceId;          // may or may not be set, purpose unknown
    string groupTag;          // GUID or strings containing guids, may or may not be set, depending on grouping
    

    
// https://github.com/memononen/nanosvg/blob/master/src/nanosvg.h ???  Or write my own?
// https://css-tricks.com/svg-path-syntax-illustrated-guide/
// https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths
// https://www.w3.org/TR/SVG/paths.html
    string dPath;                // PATH only

// dPath    "M14.8 49A13.8 13.8 0 0 1 3.3 27.7L16.7 7.3a13.8 13.8 0 0 1 23 0l13.5 20.4A13.8 13.8 0 0 1 41.7 49H14.8Z"
// dPath    "m30 47.7.4-.3h.1C46.8 36 55 25.5 55.2 16.2A15 15 0 0 0 40.6 1C36 1 31.8 3.3 29 7a1.2 1.2 0 0 1-1.9 0c-2.7-3.7-7-6-11.6-6A15 15 0 0 0 1 16.3c-.1 9.3 8 19.7 24.6 31l.1.1.4.3 2 1.3 2-1.3Z"

    charObjectArray charJSONs;      // TEXT only, version 1.1.19
    font_data fontData;             // TEXT only, version 1.1.19
    
    string layerColor;              // hex RGB value, version 1.3.21
    string layerTag;                // hex RGB value, version 1.3.21
    string fillRule;                // string "nonzero", version 1.3.21

    string originColor;             // hex RGB value, version 1.4.11
    double radius;                  // version 1.4.11
    double maxRadius;               // version 1.4.11
    
};

typedef unordered_map<GUID,object_data> objectList;

/******************************************************************************/

struct layer_data {
    layer_data() : order(1) {}
    
    string id;    // currently always a hex color  "#00c715", "#2366ff"
    string name;    // "{layer} 2", "{layer} 1",
    int order;      // 1,2,...
};

typedef unordered_map<string,layer_data> layerDataList;

/******************************************************************************/

struct canvas_struct {
    canvas_struct() : title("Canvas 1") {}

    GUID    id;
    string title;
    objectList objects;    // aka "displays"
    layerDataList   layerData;      // added in 1.3.21
};

// map would be better, but this preserves the canvas order
typedef vector<canvas_struct> canvasList;

/******************************************************************************/

struct laser_plane_struct {
    laser_plane_struct() : cushion(0), material(0), thickness(0.0),
                            diameter(-1.0), perimeter(-1.0), focalLength(0.0),
                            rotaryAttachmentType("roller"), type("LASER_PLANE") {}

    // required
    string type;            // used as key - "LASER_PLANE", "SUPER_LASER_PLANE", "LASER_CYLINDER"
    int material;
    double thickness;
    
    // optional
    int cushion;                    // CYLINDER
    double diameter;                // CYLINDER, null for not set?  depends on type of plane
    double perimeter;               // CYLINDER, null for not set?
    double focalLength;             // CYLINDER - float
    string rotaryAttachmentType;    // CYLINDER, "roller"
    string lightSourceMode;         // 1.3.21   "red"
};

/******************************************************************************/
/*
bitmapModes:  (someone read a textbook and didn't really test them all, or understand the names)
    grayscale
    Bayer
    Floyd           Floyd-Steinberg
    Stucki
    Atkinson
    Jarvis
    Sierra

density is lines/cm
    range is 20 to 300

 */
struct cut_parameter_struct {
    cut_parameter_struct() : name("customize"), repeat(1), speed(20),
                                bitmapMode("grayscale"), density(100),
                                power(1), cutPressure(100), dotDuration(10),
                                breakPointCount(0), breakPointCut(false),
                                breakPointPower(0), breakPointSize(0.5),
                                enableBreakPoint(false), dpi(0) {}
    
    cut_parameter_struct( int pwr, int rpt, int spd, string bmpMode, int dense, int pres ) :
                    power(pwr), repeat(rpt), speed(spd), bitmapMode(bmpMode), density(dense),
                    cutPressure(pres), dotDuration(10), breakPointCount(0), breakPointCut(false),
                    breakPointPower(0), breakPointSize(0.5), enableBreakPoint(false), dpi(0)
                    {
                    name = "customize";
                    }

    // required
    string name;    // matches materialType "customize"
    int repeat;
    int speed;
    
    // optional
    string bitmapMode;  // "grayscale" only for FILL_VECTOR_ENGRAVING, or BITMAP_ENGRAVING
    int density;        // only for FILL_VECTOR_ENGRAVING, or BITMAP_ENGRAVING
    int power;          // for all but KNIFE_CUTTING
    int cutPressure;    // only for KNIFE_CUTTING
    string materialType;    // only present in a few files, could be beta?
    
    string bitmapScanMode;   // 1.3.21  "zMode"
    string processingLightSource;   // 1.3.21  "blue"
    string bitmapEngraveMode;   // 1.3.21  "normal"
    
    // the breakPoint data is very poorly written, should have been an object, with default empty
    int dotDuration;            // 1.3.21  10
    int breakPointCount;        // 1.3.21  2
    bool breakPointCut;         // 1.3.21  false
    int  breakPointPower;       // 1.3.21  0
    double breakPointSize;      // 1.3.21 0.5
    bool enableBreakPoint;      // 1.3.21  false
    
    int dpi;        // 1.3.21   500
    vector<int> powerMinMaxRange; // 1.3.21 [ 1, 10 ]
};

/******************************************************************************/

// only the first 4 are typically defined
typedef enum {
    xcsFillEngraving,       // == Engrave
    xcsKnifeCut,            // == knife on M1
    xcsLaserCut,            // == Cut
    xcsVectorEngrave,       // == Score
    xcsBitmapEngraving,     // == Engrave for an image, that has additional options
} xcsCutType;

extern xcsCutType cutStringToType( const string &cut );
extern string cutTypeToString( const xcsCutType cut );
extern bool isValidCutType( const string &cut );

/******************************************************************************/

struct cut_types_struct {
    cut_types_struct() : processIgnore(false), materialType("customize") {}

    string name;            // FILL_VECTOR_ENGRAVING, KNIFE_CUTTING, VECTOR_CUTTING, VECTOR_ENGRAVING, BITMAP_ENGRAVING
    string materialType;    // "customize"
    bool processIgnore;     // false
    cut_parameter_struct parameters;
};

// currently always 4 entries (wasteful, but all types are always defined)
typedef unordered_map<string,cut_types_struct> object_cut_list;

/******************************************************************************/

struct displays_struct {
    displays_struct() : isFill(false), processIgnore(false),
                        processingType("VECTOR_ENGRAVING"), type("RECT") {}

    GUID id;                // object ID, must match canvas object definitions
    object_cut_list cut_settings;     // currently always 4 entries
    bool isFill;            // false for cuts, only possible true for FILL_VECTOR_ENGRAVING
    string processingType;  // must match one of the keys from cut name
    string type;            // "CIRCLE", "RECT", "LINE", etc.
    bool processIgnore;     // should this be ignored, or output (ie: alignment guides)
};

typedef unordered_map<GUID,displays_struct> displayList;

/******************************************************************************/

struct cut_struct {
    cut_struct() : mode("LASER_PLANE"), needUpdateCanvas(false) {}

    GUID    id;     // key in list, must match canvas GUID
    laser_plane_struct laser_plane;
    displayList displays;
    string mode;            // "LASER_PLANE", "SUPER_LASER_PLANE", "LASER_CYLINDER" must match key of laser_plane_struct
    bool needUpdateCanvas;  // false
};

typedef unordered_map<GUID,cut_struct> cutList;

/******************************************************************************/

struct localized_text {
    string locale;  // used as key
    string text;
};

typedef unordered_map<string,localized_text> localized_list;

/******************************************************************************/

struct sku_struct {
    sku_struct() : id(0) {}

    string code;
    int id;
    string key;
    string name;
};

typedef vector<sku_struct> sku_list;

/******************************************************************************/

struct code_name_struct {
    code_name_struct() : id(-1) {}
    
    string code;
    int id;
    string name;
};

/******************************************************************************/

struct machineParas_struct {
// TODO - init values!

    int bitmapModeId;
    string bladeType;
    int bladeTypeId;
    int createdAt;
    int createdBy;
    int cutPressure;
    int deletedAt;
    int densityId;
    int deviceModeId;
    int id;
    int laserPowerId;
    int materialId;
    int power;
    int processingTypeId;
    int repeat;
    int speed;
    int updatedAt;
    int updatedBy;

    code_name_struct bitmapMode;
    code_name_struct density;
    code_name_struct deviceMode;
    code_name_struct laserPower;
    code_name_struct processingType;
};

typedef vector<machineParas_struct> machineParasList;

/******************************************************************************/

struct material_struct {
// TODO - init values!

    int categoryId;
    string categoryName;
    int createdAt;
    int createdBy;
    int deletedAt;
    double diameter;
    string helpUrl;
    int id;
    string image;   // actually a URL
    bool isPreset;
    
    string saleStatus;
    string saleUrl;
    string status;
    double thickness;
    int typeId;
    string typeName;
    int updatedAt;
    int updatedBy;
    
    machineParasList machineParas;
    localized_list name;
    sku_list skus;
};

typedef vector<material_struct> materialList;

/******************************************************************************/

// an optional struct, not always present in the file
struct XCS_device {
    XCS_device() : power(-1), id("MDP") {}

    cutList cut_data;
    int power;
    materialList materialList;
    string id;          // "MDP" == User-defined material
//  vector<????>  materialTypeList;       // always empty, no idea what the contents would be

};

/******************************************************************************/

// an optional struct, not always present in the file
struct XCS_meta {
    XCS_meta() : date(-1) {}

    int date;
    string ua;
    string version;
};

typedef vector<XCS_meta> metaList;

/******************************************************************************/

void to_json( json &j, const XCS_meta &g );

/******************************************************************************/

struct XCSDocument
{
    XCSDocument() : extId("D1Pro"), version("1.0.19"), created(-1), modify(-1) {}

    canvasList canvas;
    XCS_device device;
    GUID canvasId;      // GUID for which canvas is active, must match definitions in canvas and device!
    string extId;       // machine this is targeted for "D1Pro"
    string version;     // "1.0.19", "1.1.19", "1.1.24", "1.2.14", "1.3.21"
    int created;
    int modify;
    metaList meta;
};

/******************************************************************************/

void FindDocumentBounds( XCSDocument &doc, point2D &topLeft, point2D &bottomRight);
void ExportSVGFile( std::ostream &out, XCSDocument &doc );

void ReadGUID( json &input, const char *key, GUID &result );
void ReadString( json &input, const char *key, string &result );
void ReadInt( json &input, const char *key, int &result );
void ReadLocalizedText( json &object, localized_list &names );
void ReadDouble( json &input, const char *key, double &result );
void ReadBool( json &input, const char *key, bool &result );
void ReadIntOrString( json &input, const char *key, int &result );

bool isValidCutType( const string &cut );
bool isValidCutType( const xcsCutType cut );
string cutTypeToString( const xcsCutType cut );
xcsCutType cutStringToType( const string &cut );

string NewBarelyUniqueIDString(void);

GUID XCSAddCanvas( XCSDocument &doc, string name );
void ChangeObjectCutSettings( XCSDocument &doc, GUID &object, xcsCutType cut, int power = 1, int repeat = 1, int speed = 20, string bitmapMode = "grayscale", int density = 100, string materialType = "");
void ChangeObjectCutMode( XCSDocument &doc, GUID &object, xcsCutType cut);
canvas_struct *FindCanvas( XCSDocument &doc, GUID canvas_id );
object_data *FindObject( XCSDocument &doc, GUID &object );
displays_struct *FindObjectCutSettings( XCSDocument &doc, GUID &object );

void MatchUpIDs( XCSDocument &doc, const string &filename, bool strict = false, bool cleanUpMesses = true );

void MergeXCSCanvases( std::ostream &out, const XCSDocument &doc1, XCSDocument &doc2 );

GUID XCSAddObjectRect( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut = xcsLaserCut, bool isFilled = false, GUID group = GUID() );
GUID XCSAddObjectCircle( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut = xcsLaserCut, bool isFilled = false, GUID group = GUID() );
GUID XCSAddObjectLine( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut = xcsLaserCut, GUID group = GUID() );
GUID XCSAddObjectText( XCSDocument &doc, GUID &canvas_id, point2D topLeft, double size, string text,
                    xcsCutType cut = xcsFillEngraving, bool isFilled = true, GUID group = GUID() );
GUID XCSAddObjectPen( XCSDocument &doc, GUID &canvas_id, vector<point2D> &points,
                    bool closedPath = true, bool isFilled = true,
                    xcsCutType cut = xcsLaserCut, GUID group = GUID() );
GUID XCSAddObjectPolygon( XCSDocument &doc, GUID &canvas_id, vector<point2D> &points,
                    bool isFilled = true,
                    xcsCutType cut = xcsLaserCut, GUID group = GUID() );
GUID XCSAddObjectSVGPath( XCSDocument &doc, GUID &canvas_id, string svg_path,
                    point2D topLeft, point2D size, bool isFilled = true,
                    xcsCutType cut = xcsLaserCut, GUID group = GUID() );
void SortObjectsZOrder( XCSDocument &doc, bool increasing = true );

void WriteXCSFile( std::ostream &out, XCSDocument &doc );
void ReadXCSFile( std::istream &in, XCSDocument &doc );

GUID NewBarelyUniqueIDString();
void UnitTestBase64Functions(void);
void CreateDefaultCutSettings( object_cut_list &list );

/******************************************************************************/

typedef unordered_set<string> keyList;
void ValidateKeys( json &input, const keyList &required_keys, const keyList &optional_keys, const string &description );

/******************************************************************************/

#endif /* xcs_document_h */
