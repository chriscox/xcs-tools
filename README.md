# XCS Tools
Code to read and write XCS files for the "xTool Creative Space" for xTool laser cutters.

- Reads XCS files.
- Writes XCS files.
- Validates and fixes some errors in XCS files.
- Create test grids using small JSON file to define the grids.
- Export XCS file content as SVG.
- Merge 2 XCS files into a single file (copies canvases)


# LQX Tools
Code to read LQX files and rewrite as XCS files.

- Reads LQX files, saves as XCS files.


## License
MIT License

## Project status
Still writing, could use code review.

## Authors and acknowledgment
Niels Lohmann's excellent JSON library made this much easier to develop.

xTool is a trademark of Shenzhen MasterWorks Technology Co. LTD., also doing business as MAKEBLOCK CO., LTD.
    also apparently known as SHENZHEN XTOOLTECH INTELLIGENT CO., LTD. ?
