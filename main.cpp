//
//  main.cpp
//  xcs_tools -- to read and write XCS files for xTool Creative Space
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

/*
Holy crap, someone failed their data structures class!


Units appear to be in millimeters
Coordinates are from top left

The files have lots of floating point rounding errors in printing or processing

fillcolor/lineColor change with engrave/cut/score settings
fillColor/lineColor are integer versions of hex RGB
    16421416 = 0xFA9228 = orangeish / score - as linecolor
    10036991 = 0x9926ff = violet / cut -- as linecolor
    16421416 = 0xFA9228 = orangeish / engrave - as fill and linecolor

XCS is leaving behind unused device canvas and cut items (not defined in displays)

XCS reads several image formats (PNG, JPEG, BMP, GIF), but all get converted to PNG in the file

Reflection/flip handling is bizarre, like someone didn't understand the math and geometry.





TODO - object model code
    object manipulation - location, angle, Zorder, etc.

TODO - verify parameter ranges as well as keys
    currently not done, and some beta files have bogus values

TODO - verify parameter types as well as keys?
    already done when reading values - so minimal benefit


*/

#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <filesystem>
#include "xcs_document.hpp"
#include "Grids.hpp"

/******************************************************************************/

using namespace std;
namespace fs = std::filesystem;

/******************************************************************************/
/******************************************************************************/

// orientation always has first point upward
// indent is a fraction
void CreateStarShape( point2D center, double radius, int points, double indent, vector<point2D> &out )
{
    double angleIncrement = 2*M_PI / points;
    double indentRadius = radius * indent;
    
    out.clear();
    out.reserve(2*points);

    for (int i = 0; i < points; ++i)
        {
        double angle1 = double(i) * angleIncrement;
        double angle2 = (double(i)+0.5) * angleIncrement;
        
        double x1 = radius * sin(angle1);
        double y1 = -radius * cos(angle1);
        point2D aPoint(x1,y1);
        
        double x2 = indentRadius * sin(angle2);
        double y2 = -indentRadius * cos(angle2);
        point2D indentPoint(x2,y2);
        
        out.push_back(aPoint+center);
        out.push_back(indentPoint+center);
        }
}

/******************************************************************************/

void CreateTestFile( const char *outfilename )
{
    XCSDocument doc;

    // new canvas
    GUID canvas1 = XCSAddCanvas( doc, "testCanvas1" );
    
    // new objects
    GUID rect1 = XCSAddObjectRect( doc, canvas1, point2D(0,0), point2D(inch,inch) );
    GUID circ1 = XCSAddObjectCircle( doc, canvas1, point2D(2*inch,2*inch), point2D(inch,inch*2) );
    GUID line1 = XCSAddObjectLine( doc, canvas1, point2D(6*inch,2*inch), point2D(0,inch*2) );
    GUID line2 = XCSAddObjectLine( doc, canvas1, point2D(6*inch,2*inch), point2D(inch*2,0) );
    GUID line3 = XCSAddObjectLine( doc, canvas1, point2D(6*inch,2*inch), point2D(inch*2,inch*2) );
    
    // change object cut specs
    ChangeObjectCutSettings( doc, rect1, xcsLaserCut, 100, 1, 50 );
    
    // new text
    GUID text1 = XCSAddObjectText( doc, canvas1, point2D(inch, inch), 14, "testing123!", xcsFillEngraving  );
    // object_data *text_data = FindObject( doc, text1 );
    
    // reads down
    GUID text90 = XCSAddObjectText( doc, canvas1, point2D(inch*3, inch), 14, "rotate90", xcsFillEngraving  );
    object_data *text_data90 = FindObject( doc, text90 );
    text_data90->angle = 90;
    
    // reads upside down
    GUID text180 = XCSAddObjectText( doc, canvas1, point2D(inch*4, inch), 14, "rotate180", xcsFillEngraving  );
    object_data *text_data180 = FindObject( doc, text180 );
    text_data180->angle = 180;
    
    // reads up
    GUID text270 = XCSAddObjectText( doc, canvas1, point2D(inch*5, inch), 14, "rotate270", xcsFillEngraving );
    object_data *text_data270 = FindObject( doc, text270 );
    text_data270->angle = 270;
    
    
    // pen shape (triangle)
    vector<point2D> penPath = { {25,100}, {25,150}, {110,150} } ;
    GUID pen = XCSAddObjectPen( doc, canvas1, penPath, true, false, xcsLaserCut );
    
    // polygon shape (star)
    vector<point2D> starPath;
    CreateStarShape( point2D(300,100), 35, 5, 0.40, starPath);
    GUID poly = XCSAddObjectPolygon( doc, canvas1, starPath, false, xcsLaserCut );

    CreateStarShape( point2D(250,50), 25, 21, 0.3, starPath);
    GUID poly2 = XCSAddObjectPolygon( doc, canvas1, starPath, false, xcsLaserCut );
    
    
    // SVG path shape (heart)
    string svgString = "m30 47.7.4-.3h.1C46.8 36 55 25.5 55.2 16.2A15 15 0 0 0 40.6 1C36 1 31.8 3.3 29 7a1.2 1.2 0 0 1-1.9 0c-2.7-3.7-7-6-11.6-6A15 15 0 0 0 1 16.3c-.1 9.3 8 19.7 24.6 31l.1.1.4.3 2 1.3 2-1.3Z";
    XCSAddObjectSVGPath( doc, canvas1, svgString, point2D(320, 25), point2D(40, 40), true, xcsFillEngraving  );
    
    
    // bitmap?
    
    
    // verify
    MatchUpIDs( doc, outfilename, true, true );
    
    // and save
    std::ofstream out( outfilename );
    WriteXCSFile( out, doc );
    out.close();
}

/******************************************************************************/
/******************************************************************************/

void ReadValidateRewrite( const string &infilePath, const string &outfilename )
{
    std::ifstream f( infilePath );

    XCSDocument doc;
    ReadXCSFile( f, doc );
    f.close();
    
    fs::path temppath( infilePath );
    string pathtemp = temppath.filename().string();
    
    MatchUpIDs( doc, pathtemp, false, true );   // data read is from XCS, may be messy

    std::ofstream out( outfilename );
    WriteXCSFile( out, doc );
    out.close();
}

/******************************************************************************/

void
RegressionTestFiles()
{
    const char *testfiles[] = {
        "../../TwoCirclesTwoSquares.xcs",
        "../../Groups.xcs",
        "../../MultipleShapes.xcs",
        "../../testfiles/10WPro_ThinCutTest.xcs",
        "../../testfiles/Beech Rolling Pin.xcs",    // image
        "../../testfiles/Christmas ornament.xcs",
        "../../testfiles/City Map.xcs",
        "../../testfiles/Colorful Wooden Mushroom Ring.xcs",
        "../../testfiles/Father and Son.xcs",
        "../../testfiles/FeltFlowers.xcs",
        "../../testfiles/Kraft Paper Figurine.xcs",
        "../../testfiles/Canvas Bag.xcs",
        "../../testfiles/Dice Tower.xcs",
        "../../testfiles/Halloween Pumpkin Notch.xcs",
        "../../testfiles/Peace and Love Earrings.xcs",
        "../../testfiles/ReflectedText.xcs",
        "../../testfiles/Rotations.xcs",
        "../../testfiles/Tiny Wooden Horse.xcs",
        "../../testfiles/St. Patrick's Day Decoration Plaque.xcs",
        "../../testfiles/Stainless Steel Tumbler.xcs",  // image
        "../../testfiles/ChristmasTreeBeerHolder.xcs",
        "../../testfiles/FeltSunflowers.xcs",
        "../../testfiles/AnnularBackflowIncense.xcs",
        "../../testfiles/CandleLampshade.xcs",
        "../../testfiles/KraftPaperNightlight.xcs",
        "../../testfiles/keychain.xcs",
        "../../testfiles/Graduation.xcs",
        "../../testfiles/wooden aircraft.xcs",
        "../../testfiles/graduation cap.xcs",
        "../../testfiles/Dinosaur.xcs",
        "../../testfiles/Bamboo Gift Box.xcs",
        "../../testfiles/Furniture decoration.xcs",
        "../../testfiles/pumpkin candy bag.xcs",
        "../../testfiles/bouquet.xcs",
        
        
        "../../testfiles/rubber stamp1019.xcs",      // image, 44k
        "../../testfiles/rubber stamp1119.xcs",      // 45k     no change to image stored, same on other files
        "../../testfiles/rubber stamp1214.xcs",      // 46k     no change to image stored, same on other files
        "../../testfiles/rubber stamp1321.xcs",      // 46k     no change to image stored, same on other files
        "../../testfiles/rubber stamp1411.xcs",      // 46k     no change to image stored, same on other files
        
        "../../testfiles/MultipleShapes1019.xcs",    // 41k
        "../../testfiles/MultipleShapes1119.xcs",    // 134k WTAF?   text and font descriptions in SVG form, very redundant
        "../../testfiles/MultipleShapes1124.xcs",    // 144k WTAF?   text and font descriptions in SVG form, very redundant
        "../../testfiles/MultipleShapes1214.xcs",    // 144k WTAF?   text and font descriptions in SVG form, very redundant
        "../../testfiles/MultipleShapes1321.xcs",    // 152k WTAF?   text and font descriptions in SVG form, very redundant
        "../../testfiles/MultipleShapes1411.xcs",    // 167k WTAF?   text and font descriptions in SVG form, very redundant
    };

    for( auto &path : testfiles )
        {
        
        // read, and continue if file not found
        std::ifstream f;
        bool valid = true;
        f.exceptions( std::ifstream::failbit | std::ifstream::badbit );
        try {
            f.open(path);
        }
        catch (std::system_error& e) {
            std::cerr << "Could not open " << path << '\n';
            valid = false;
        }
        
        if (!valid)
            continue;
        
        XCSDocument doc;
        ReadXCSFile( f, doc );
        f.close();
        
        fs::path temppath( path );
        string pathtemp = temppath.filename().string();
        
        // check IDs match, and remove extra items that don't match
        MatchUpIDs( doc, pathtemp, false, true);   // data read is from XCS, may be messy
        
        // make new filename without path
        string outfilename = string("rewrite_") + pathtemp;
        
        // write
        std::ofstream out( outfilename );
        WriteXCSFile( out, doc );
        out.close();

#if 1
        fs::path tempstem = temppath.stem();
        string outfilename3 = string(tempstem) + ".svg";
        std::ofstream out_svg( outfilename3 );
        ExportSVGFile( out_svg, doc );
        out_svg.close();
#endif

#if 1
        // error check by reading back in, writing out again
        std::ifstream verify( outfilename );
        XCSDocument doc2;
        ReadXCSFile( verify, doc2 );
        verify.close();
        
        MatchUpIDs( doc2, outfilename, true, true );    // now the data read is ours, should be clean
        
        string outfilename2 = string("rewrite2_") + pathtemp;
        std::ofstream out2( outfilename2 );
        WriteXCSFile( out2, doc2 );
        out2.close();
#endif
        
        }
}

/******************************************************************************/

void
RegressionTestGridParams()
{
    const char *gridDefs[] = {
        "../../BasicEngraveGrid.json",
        "../../DelicateEngraveGrid.json",
        "../../ThickCutGrid.json",
        "../../ThinCutGrid.json",
    };

    for( auto &path : gridDefs )
        {
        std::ifstream in( path );
        json newGrid = json::parse(in);
        in.close();
        
        gridDefinition definition = newGrid.get<gridDefinition>();
        
        assert( definition.isValid(true) );
        
        json debugGrid = definition;
        
        fs::path temppath( path );
        fs::path tempstem = temppath.stem();
        string outfilename = string(tempstem) + ".xcs";
        CreateEngraveGrid( outfilename, definition );
        
        std::ofstream out( "debugGrid.json" );
        out << debugGrid.dump(4);
        out.close();
        
        gridDefinition definition2 = debugGrid.get<gridDefinition>();
        assert( definition == definition2 );
    }
    
    remove( "debugGrid.json" );
}

/******************************************************************************/
/******************************************************************************/

struct globalParameters {
    globalParameters() : do_regression(false), have_input(false), have_grid(false),
                have_extract(false), have_merge(false),
                outputFile( "outfile.xcs" ) {}

    bool do_regression;
    bool have_input;
    bool have_grid;
    bool have_extract;
    bool have_merge;
    
    string inputFile;   // also mergefile1
    string gridFile;
    string outputFile;
    string extractFile;
    string mergeFile2;
};

/******************************************************************************/

void
print_usage(const char *progname)
{
	fprintf(stderr, "\n\n");
	fprintf(stderr, "usage:\n%s\tout_file\n", progname );
	fprintf(stderr, "\t[-i infile.xcs]\n" );
	fprintf(stderr, "\t[-grid grid_input.json]\n");
	fprintf(stderr, "\t[-extract infile.xcs]\n");
	fprintf(stderr, "\t[-merge infile1.xcs infile2.xcs]\n");
	printf("Compiled %s %s\n", __DATE__, __TIME__ );
}

/******************************************************************************/

void parse_arguments( int argc, const char *argv[], globalParameters &params )
{
	for ( int c = 1; c < argc; ++c )
		{
		
        if ( (strcmp( argv[c], "-i" ) == 0 || strcmp( argv[c], "-I" ) == 0 )
            && c < (argc-1)  )
            {
            params.inputFile = argv[c+1];
            params.have_input = true;
            ++c;
            }
        else if ( (strcmp( argv[c], "-grid" ) == 0 || strcmp( argv[c], "-GRID" ) == 0 )
            && c < (argc-1)  )
            {
            params.gridFile = argv[c+1];
            params.have_grid = true;
            ++c;
            }
        else if ( (strcmp( argv[c], "-extract" ) == 0 || strcmp( argv[c], "-EXTRACT" ) == 0 )
            && c < (argc-1)  )
            {
            params.extractFile = argv[c+1];
            params.have_extract = true;
            ++c;
            }
        else if ( (strcmp( argv[c], "-merge" ) == 0 || strcmp( argv[c], "-MERGE" ) == 0 )
            && c < (argc-2)  )
            {
            params.inputFile = argv[c+1];
            params.mergeFile2 = argv[c+2];
            params.have_merge = true;
            c += 2;
            }
        else if ( (strcmp( argv[c], "-regression" ) == 0 || strcmp( argv[c], "-GRID" ) == 0 ) )
            {
            params.do_regression = true;
            ++c;
            }
		else if ( strcmp( argv[c], "-V" ) == 0
				|| strcmp( argv[c], "-v" ) == 0
				|| strcmp( argv[c], "-help" ) == 0
				|| strcmp( argv[c], "--help" ) == 0
				|| strcmp( argv[c], "-version" ) == 0
				|| strcmp( argv[c], "-Version" ) == 0
				)
			{
			print_usage( argv[0] );
			exit(0);
			}
		else if (argv[c][0] == '-')
			{
			// unrecognized switch
			print_usage( argv[0] );
			exit(1);
			}
        else {
            // no switch, treat as output filename
            params.outputFile = argv[c];
            }
		
		}   // for args
}

/******************************************************************************/

int
main(int argc, const char * argv[])
{

    // parse command line arguments
    globalParameters params;
	parse_arguments( argc, argv, params );


    if ( params.do_regression )
        {
        // developers need to rerun tests after any major changes
        cout << "Running regression tests\n";
        UnitTestBase64Functions();
        RegressionTestFiles();
        RegressionTestGridParams();
        CreateTestFile( "constructed.xcs" );
        }


    if ( params.have_input )
        {
        cout << "Cleaning " << params.inputFile << "\n";
        ReadValidateRewrite( params.inputFile,  params.outputFile );
        }


    if ( params.have_grid )
        {
        cout << "Creating test grid from " << params.gridFile << "\n";
        std::ifstream in( params.gridFile );
        json newGrid = json::parse(in);
        in.close();
        
        gridDefinition definition = newGrid.get<gridDefinition>();
        CreateEngraveGrid(  params.outputFile, definition );
        }

    if (params.have_extract)
        {
        //params.extractFile = "./outfile.xcs";

//params.extractFile = "../../testfiles/ReflectedText.xcs";

//params.extractFile = "../../testfiles/Christmas ornament.xcs";

//params.extractFile = "../../MultipleShapes.xcs";

//params.extractFile = "../../testfiles/Rotations.xcs";       // paths are still a tiny bit off position

        cout << "Exporting SVG from " << params.extractFile << "\n";
        std::ifstream f( params.extractFile );
        XCSDocument doc;
        ReadXCSFile( f, doc );
        f.close();
        
        std::ofstream out( params.outputFile );
        ExportSVGFile( out, doc );
        out.close();
        }


    if (params.have_merge)
        {
        cout << "Merging from " << params.inputFile << " and " << params.mergeFile2 << "\n";
        std::ifstream f1( params.inputFile );
        XCSDocument doc1;
        ReadXCSFile( f1, doc1 );
        f1.close();
        
        std::ifstream f2( params.mergeFile2 );
        XCSDocument doc2;
        ReadXCSFile( f2, doc2 );
        f2.close();

        MatchUpIDs( doc1, params.inputFile, false, true );
        MatchUpIDs( doc2, params.mergeFile2, false, true );
        
        std::ofstream out( params.outputFile );
        MergeXCSCanvases( out, doc1, doc2 );
        out.close();
        }

    return 0;
}

/******************************************************************************/
/******************************************************************************/
