//
//  lqx_main.cpp
//  lqx_tools -- to read LQX files from LaserBox and write XCS files for xTool Creative Space
//
//  Created by Chris Cox on Jan 5, 2023.
//  Copyright 2023, by Chris Cox.
//

/*

LQX files - gzipped JSON
    VERY verbose, some similarity to XCS, but different in many areas
    looks like they write out all values, even when not applicable
    far more transform detail
    far more text layout detail
    Illustrator/PostScript style stroke/fill definitions
    PATH appears to be modified SVG broken up into operation arrays
    seems to use PATH for ellipses/circles (lots of arcs)
    has more GUID matched styles and metadata (still a bad idea)
    Guess they had to gzip the files because they were just too verbose
        fortunately there is a ton of reundant data in the files

*/

#include <cstdint>
#include <typeinfo>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <filesystem>
#include <chrono>
#include <algorithm>
#include "json.hpp"
#include "xcs_document.hpp"

// https://github.com/bisqwit/TinyDeflate
// This code is byte order unsafe, and will only work on little-endian systems.
// Cannot create a stream, because it maintains no state between calls.
#include "gunzip.hh"

/******************************************************************************/

using namespace std;
namespace fs = std::filesystem;

/******************************************************************************/

void VerifyLQXCutData( json &object )
{
    keyList requiredSettingParamKeys = { "power", "speed" };
    keyList optionalSettingParamKeys = { "fillUseful", "veins", "density",
            "charSpacing", "fontFamily", "fontSize", "lineHeight", "ofPasses" };
    ValidateKeys( object, requiredSettingParamKeys, optionalSettingParamKeys, "LQX cut data" );
}

/******************************************************************************/

void VerifyLQXEngraveData( json &object )
{
    keyList requiredSettingParamKeys = { "speed" };
    keyList optionalSettingParamKeys = { "fillUseful", "depth", "pressure", "ofPasses", "power",
                "contrast", "density", "engraveMode", "filter", "lineDensity",
                "res", "veins", "bitmapEngrave", "grayscaleMaximum", "grayscaleMinimum",
                "sharpness",
                "charSpacing", "fontFamily", "fontSize", "lineHeight"
    };
    ValidateKeys( object, requiredSettingParamKeys, optionalSettingParamKeys, "LQX engrave data" );
}

/******************************************************************************/

void VerifyLQXKnifeData( json &object )
{
    keyList requiredSettingParamKeys = { "ofPasses", "speed" };
    keyList optionalSettingParamKeys = { "pressure", "power" };
    ValidateKeys( object, requiredSettingParamKeys, optionalSettingParamKeys, "LQX knife data" );
}

/******************************************************************************/

void VerifyLQXCutProperty( json &object )
{
    keyList requiredSettingParamKeys = { "height", "width" };
    keyList optionalSettingParamKeys = { "charSpacing", "fontFamily", "lineHeight", "isLock", "strokeDashArrayType", "power", "speed", "material", "thickness", "ditherMode", "engraveMode", "filter", "lineDensity" };
    ValidateKeys( object, requiredSettingParamKeys, optionalSettingParamKeys, "LQX cut property" );
}

/******************************************************************************/

void VerifyLQXCutSettings( json &object )
{
    keyList requiredSettingParamKeys = { "engrave", "property", "size", "style", "transform" };
    keyList optionalSettingParamKeys = { "cut", "operationType", "color", "knife", "fillUseful",
            "name", "naturalHeight", "naturalWidth", "option", "advanced", "initType"
    };
    ValidateKeys( object, requiredSettingParamKeys, optionalSettingParamKeys, "LQX cut settings" );
}

/******************************************************************************/

void VerifyLQXCuts( json &object )
{
    keyList requiredCutParamKeys = { "id", "picType", "setting",  };
    keyList optionalCutParamKeys = { };
    ValidateKeys( object, requiredCutParamKeys, optionalCutParamKeys, "LQX cut parameters" );
}

/******************************************************************************/

void VerifyLQXObject( json &object )
{
    keyList requiredObjectKeys = { "angle", "height", "width", "top", "left", "backgroundColor",
                            "fill", "fillRule", "flipX", "flipY",
                            "globalCompositeOperation", "minScaleLimit", "opacity", "originX", "originY",
                            "paintFirst", "scaleX", "scaleY", "shadow", "skewX", "skewY",
                            "stroke", "strokeDashArray", "strokeLineCap", "strokeLineJoin", "strokeMiterLimit",
                            "strokeWidth", "transformMatrix", "type", "version",
                            "visible"
    };
    keyList optionalObjectKeys = { "id", "charSpacing", "fontFamily", "fontSize", "fontStyle", "fontWeight",
                                "lineHeight", "linethrough", "overline", "styles", "text", "textAlign",
                                "textBackgroundColor", "underline", "path", "radius", "endAngle", "startAngle",
                                "originSrc", "src", "initFill", "tmpFill", "tmpStroke", "initType", "lockUniScaling",
                                "clipTo", "objects", "pathValid", "cropX", "cropY", "crossOrigin", "filters",
                                "isClosed", "isPen", "points"
    };
    ValidateKeys( object, requiredObjectKeys, optionalObjectKeys, "object" );
}

/******************************************************************************/

void VerifyLQXCanvas( json &in )
{
    keyList requiredCanvasKeys = { "objects" };
    keyList optionalCanvasKeys = { };
    ValidateKeys( in, requiredCanvasKeys, optionalCanvasKeys, "canvas" );
}

/******************************************************************************/

void VerifyLQXFile( json &root_data )
{
    keyList requiredDocumenteKeys = { "canvasData", "settingData", "machineType", "signedHash"};
    keyList optionalDocumentKeys = { "extraData" };
    ValidateKeys( root_data, requiredDocumenteKeys, optionalDocumentKeys, "LQXfile" );
}

/******************************************************************************/

void ReadLQXCutData( json &object, displays_struct &temp )
{
    VerifyLQXCutData( object );
    
    cut_types_struct cut;
    
    cut.name = "VECTOR_CUTTING";

// density -- always 50, ignore
// fillUseful -- ??????

    ReadInt( object, "ofPasses", cut.parameters.repeat );
    ReadInt( object, "power", cut.parameters.power );
    ReadInt( object, "speed", cut.parameters.speed );

// veins - dither pattern? -- ignore

    temp.cut_settings[ cut.name ] = cut;
}

/******************************************************************************/

void ReadLQXEngraveData( json &object, displays_struct &temp )
{
    VerifyLQXEngraveData( object );
    
    cut_types_struct cut;
    
    cut.name = "VECTOR_ENGRAVING";

// fillUseful -- ??????
// pressure?
// contrast?
// engraveMode?
// res?
// veins?  probably dither pattern/mode

    ReadInt( object, "ofPasses", cut.parameters.repeat );
    ReadInt( object, "power", cut.parameters.power );
    ReadInt( object, "speed", cut.parameters.speed );
    
    // someone fails at JSON, written as strings and ints
    ReadIntOrString( object, "lineDensity", cut.parameters.density );

    temp.cut_settings[ cut.name ] = cut;
    
    
    // since LQX doesn't easily differentiate score versus engrave, we'll define both
    cut.name = "FILL_VECTOR_ENGRAVING";

    temp.cut_settings[ cut.name ] = cut;
}

/******************************************************************************/

void ReadLQXKnifeData( json &object, displays_struct &temp )
{
    VerifyLQXKnifeData( object );
    
    cut_types_struct cut;
    
    cut.name = "KNIFE_CUTTING";

    ReadInt( object, "ofPasses", cut.parameters.repeat );
    ReadInt( object, "power", cut.parameters.power );
    ReadInt( object, "pressure", cut.parameters.cutPressure );
    ReadInt( object, "speed", cut.parameters.speed );

    temp.cut_settings[ cut.name ] = cut;
}

/******************************************************************************/

void ReadLQXCutProperty( json &object, displays_struct &temp )
{
    VerifyLQXCutProperty( object );

// height, width - duplicate object, or override? -- ignore
// material - always 1 -- ignore
// power - duplicate cut and engrave, or override? -- ignore
// speed - duplicate cut and engrave, or override? -- ignore
// thickness - always 0 -- ignore

// ignore all the optional values, seem to be duplicates of object values (possibly overrides?)
}

/******************************************************************************/

void ReadLQXCutSettings( json &object, displays_struct &display_data )
{
    VerifyLQXCutSettings( object );

// option appears to duplicate operationType, sometimes
    string tempOperation;
    ReadString( object, "operationType", tempOperation);
    if (tempOperation == string())
        ReadString( object, "option", tempOperation);
    
    if (tempOperation == "cut")
        {
        display_data.processingType = "VECTOR_CUTTING";
        display_data.isFill = false;
        }
    else if (tempOperation == "engrave")
        {
// How does LQX differentiate between fill and stroke engraving?
// Seems to always stroke engrave unless using an image.
        display_data.processingType = "VECTOR_ENGRAVING";
        display_data.isFill = false;
        }
    else if (tempOperation == "knife")
        {
        display_data.processingType = "KNIFE_CUTTING";
        display_data.isFill = false;
        }
    else
        {
        if (tempOperation != string())
            cout << "unknown LQX operation type: " << tempOperation << "\n";
        
        // default for cases where operation is missing (yep, it happens)
        display_data.processingType = "VECTOR_CUTTING";
        display_data.isFill = false;
        }

// advanced seems to override some cut values, it just doesn't define which -- ignore for now

    CreateDefaultCutSettings( display_data.cut_settings );

    auto cut_params = object.find("cut");
    if (cut_params != object.end())
        ReadLQXCutData( cut_params.value(), display_data );

    auto engrave_params = object.find("engrave");
    if (engrave_params != object.end())
        ReadLQXEngraveData( engrave_params.value(), display_data );

    auto knife_params = object.find("knife");
    if (knife_params != object.end())
        ReadLQXKnifeData( knife_params.value(), display_data );

    auto property_params = object.find("property");
    if (property_params != object.end())
        ReadLQXCutProperty( property_params.value(), display_data );


// size - usually zero -- ignore
// style -- ignore
// transform - so far always NULL -- ignore

}

/******************************************************************************/

void ReadLQXCuts( json &object, XCSDocument &doc )
{
    doc.device.power = 0;
    // leave device.materialList empty (default)
    // leave device.id at default
    
    cut_struct temp;
    temp.id = doc.canvasId;
    // leave laser_plane at default
    // leave mode at default
    // leave needUpdateCanvas at default
    

    for ( auto& [key, value] : object.items())
        {
        VerifyLQXCuts( value );
        
        displays_struct display;

        ReadGUID( value, "id" , display.id );

        object_data *obj = FindObject( doc, display.id );
        assert( obj != NULL );
        display.type = obj->type;
        
        string tempPicType;
        ReadString( value, "picType", tempPicType);
        if (tempPicType != "vector" && tempPicType != "bitmap")
            {
            cout << "unknown LQX picType: " << tempPicType << "\n";
            }

        auto setting = value.find("setting");
        if (setting != value.end())
            ReadLQXCutSettings( setting.value(), display );

         temp.displays[ display.id ] = display;
        }
    
    doc.device.cut_data[ temp.id ] = temp;
}

/******************************************************************************/

// we want to know if the value is there or NULL, don't care about value
bool IsValueNotNull( json &input, const char *key )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto type = dataFound.value().type();
        if (type != json::value_t::null )
            return true;
        }
    
    return false;
}

/******************************************************************************/

// key "path" is an array of arrays of SVG operations
// we need to concatenate to a normal SVG string
// always starts with operation letter, followed by variable numbers
string parseLQXPath( json &object )
{
    string pathString;

    auto pathArray = object.find("path");
    if (pathArray != object.end())
        {
        for ( auto& [key, op] : pathArray.value().items())
            {
            //bool firstDone = false;
            for ( auto& [key2, param] : op.items())
                {
                if (param.type() == json::value_t::string)
                    {
                    string temp = param;
                    pathString += temp;
                    }
                else
                    pathString += " " + to_string(param);
                }
            }
        }

    // should always end with "z"
    return pathString;
}

/******************************************************************************/

void ReadLQXObject( json &object, object_data &obj, XCSDocument &doc )
{
    VerifyLQXObject( object );

    ReadGUID( object, "id", obj.id );
    
    ReadDouble( object, "angle", obj.angle );
    ReadDouble( object, "height", obj.height );
    ReadDouble( object, "width", obj.width );
    ReadDouble( object, "left", obj.x );
    ReadDouble( object, "top", obj.y );
    
// backgroundColor -- ignore

// fill -- if value, is filled, if empty not filled
// stroke -- if value, is not filled, if empty is filled -- ignore?
// initFill -- equals fill -- ignore
    obj.isFill = IsValueNotNull( object, "fill" );

// fillRule -- ignore

    ReadBool( object, "lockUniScaling", obj.lockRatio );

// globalCompositeOperation -- ignore
// minScaleLimit -- ignore
// opacity -- ignore
// originX, originY -- always "left" and "top" so far
// paintFirst -- ignore

    ReadBool( object, "pathValid", obj.isClosePath );       // not sure about this ??????

// shadow -- ignore
// strokeDashArray -- ignore
// strokeLineCap -- ignore
// strokeLineJoin -- ignore
// strokeMiterLimit -- ignore
// strokeWidth -- ignore
// tmpFill, tmpStroke - always null -- ignore
// transformMatrix - always null -- ignore

    ReadDouble( object, "scaleX", obj.scale.x );
    ReadDouble( object, "scaleY", obj.scale.y );
    ReadDouble( object, "skewX", obj.skew.x );
    ReadDouble( object, "skewY", obj.skew.y );
    
    ReadString( object, "type", obj.type );     // currently "text" or "path"

// version -- ignore, currently always "2.0.3"
// visible -- ignore



// fill in some values that don't exist in LQX
    obj.offsetX = obj.x;
    obj.offsetY = obj.y;
    
    obj.pivot.x = obj.x;
    obj.pivot.y = obj.y;

// TODO - localSkew! ?           leave it zero for now, need to read flipX, flipY


    // handle object type specific values
    if (obj.type == "text" || obj.type == "i-text")
        {
        obj.type = "TEXT";
        
        ReadString( object, "text", obj.text );

        ReadDouble( object, "charSpacing", obj.style.letterSpacing );
        ReadString( object, "fontFamily", obj.style.fontFamily );

// TODO -- is this in points or millimeters
// apparently fontSize is ALWAYS 20, and the real size comes from the height value? That's insane.
        ReadDouble( object, "fontSize", obj.style.fontSize );

// TODO - there is no great way to map these!    LQX uses 2 values, XCS uses a single one (weight style)?
// LQX uses lower case, XCS uses upper case
        ReadString( object, "fontStyle", obj.style.fontSubfamily );     // Italic?
        ReadString( object, "fontWeight", obj.style.fontSubfamily );    // bold, black, etc.
        
        ReadDouble( object, "lineHeight", obj.style.leading );
        ReadString( object, "textAlign", obj.style.align );

// "styles" -- always empty struct, ignore
// linethrough -- ignore
// overline -- ignore
// textBackgroundColor" -- ignore
// underline -- ignore
        }
    else if (obj.type == "path")
        {
        // path is an array of arrays, with each SVG op
        obj.type = "PATH";
        obj.graphicX = obj.x;
        obj.graphicY = obj.y;
        obj.dPath = parseLQXPath( object );
    
// isPen -- for editing info -- ignore
// points - seems to be editing only -- ignore
        // only present for pen tool objects, but important for those objects
        ReadBool( object, "isClosed", obj.isClosePath );
        }
    else if (obj.type == "circle")
        {
        obj.type = "CIRCLE";
        //double radius = 0.0;
        //ReadDouble( object, "radius", radius );
        // 2*radius == width == height
        // XCS uses bounds to define circle/ellipse
        }
    else if (obj.type == "image" || obj.type == "LaserImage")
        {
        obj.type = "BITMAP";
        // both keys get used, not sure why
        ReadString( object, "originSrc", obj.base64data );
        ReadString( object, "src", obj.base64data );
        }
    else
        {
        cout << "unknown LQX object type: " << obj.type << '\n';
        }
    
    // sub objects don't have an ID, or associated cut settings
    // see "laser gradient test grid rev 3.lqx"
    if (!object.contains("id") || obj.id == GUID())
        {
        obj.id = NewBarelyUniqueIDString();
        
        // create matching default cut settings
        displays_struct new_display_struct;
        new_display_struct.id = obj.id;
        new_display_struct.isFill = obj.isFill;
        new_display_struct.processingType = "VECTOR_CUTTING";
        new_display_struct.type = obj.type;
        CreateDefaultCutSettings( new_display_struct.cut_settings );
        
        // add object to cut list
        auto canvas_id = doc.canvasId;
        doc.device.cut_data[ canvas_id ].displays[ obj.id ] =  new_display_struct;
        }

    // objects is evil, it's a list of objects inside this object -- handled outside of this function

}

/******************************************************************************/

template<typename T>
void ReadLQXCanvas( T input, XCSDocument &doc )
{
    VerifyLQXCanvas( input );
    
    auto objects = input.find("objects");
    if (objects != input.end())
        {
        auto &object_list = doc.canvas[0].objects;
        
        for ( auto& [key, value] : objects.value().items())
            {
            object_data obj;
            ReadLQXObject( value, obj, doc );
            object_list.insert( { obj.id, obj } );
            
            // Sigh, iterate sub objects:  very poor design
            // Could be a poor grouping mechanism, I guess.
            // Sub objects do not have GUID ids!
            auto sub_objects = value.find("objects");
            if (sub_objects != value.end())
                {
                for ( auto& [key2, value2] : sub_objects.value().items())
                    {
                    object_data obj2;
                    ReadLQXObject( value2, obj2, doc );
                    object_list.insert( { obj2.id, obj2 } );
                    
                    if (value2.contains("objects"))
                        {
                        cout << "WARNING - recursive sub objects found!\n";
                        }
                    }
                }
                
            }
        }
}

/******************************************************************************/

void ReadLQXFile( std::istream &in, XCSDocument &doc )
{
    json root_data = json::parse(in);

#if 0
    // make sure the parser outputs what it read in without errors, and give me something more readable to debug
    {
    std::ofstream debug( "debugJSON.json");
    debug << root_data.dump(2);
    debug.close();
    }
#endif


/*
LQX structure

All objects are on a single page/canvas

{
    machineType: "education"
    signedHash: "sign"
    canvasData: {
        objects: [
            {
                "angle": 0,
                "backgroundColor": "",
                "charSpacing": 0,
                "fill": "#FD9328",
                "fillRule": "nonzero",
                "flipX": false,
                "flipY": false,
                "fontFamily": "Adelon",
                "fontSize": 4.93888855,
                "fontStyle": "normal",
                "fontWeight": "bold",
                "globalCompositeOperation": "source-over",
                "height": 5.5809440615,
                "id": "8be4d1e0-2a97-11ec-9537-25e4a3c7c12e",       object_GUID
                "initFill": "#FD9328",
                "left": 667.1226855767,
                "lineHeight": 1.16,
                "linethrough": false,
                "lockUniScaling": false,
                "minScaleLimit": 0,
                "opacity": 1,
                "originX": "left",
                "originY": "top",
                "overline": false,
                "paintFirst": "fill",
                "pathValid": true,
                "scaleX": 2,
                "scaleY": 2,
                "shadow": null,
                "skewX": 0,
                "skewY": 0,
                "stroke": "#FD9328",
                "strokeDashArray": null,
                "strokeLineCap": "butt",
                "strokeLineJoin": "miter",
                "strokeMiterLimit": 4,
                "strokeWidth": 1,
                "styles": {},               // only on text
                "text": "25",
                "textAlign": "left",
                "textBackgroundColor": "",
                "tmpFill": null,
                "tmpStroke": null,
                "top": 454.05807691,
                "transformMatrix": null,
                "type": "text",
                "underline": false,
                "version": "2.0.3",
                "visible": true,
                "width": 5.92666626
            },
            {
                "angle": 0,
                "backgroundColor": "",
                "fill": "",
                "fillRule": "nonzero",
                "flipX": false,
                "flipY": false,
                "globalCompositeOperation": "source-over",
                "height": 210,
                "id": "8be52000-2a97-11ec-9537-25e4a3c7c12e",       object_GUID
                "initFill": "",
                "left": 262,
                "lockUniScaling": false,
                "minScaleLimit": 0,
                "opacity": 1,
                "originX": "left",
                "originY": "top",
                "paintFirst": "fill",
                "path": [                           SVG decomposed into operations, can probably rejoin with spaces
                    [
                        "M",
                        10.5,
                        64.370079
                    ],
                    [
                        "H",
                        228
                    ],
                    [
                        "V",
                        274.370079
                    ],
                    [
                        "H",
                        10.5
                    ],
                    [
                        "z"
                    ]
                ],
                "pathValid": true,
                "scaleX": 2,
                "scaleY": 2,
                "shadow": null,
                "skewX": 0,
                "skewY": 0,
                "stroke": "#8800FF",
                "strokeDashArray": null,
                "strokeLineCap": "butt",
                "strokeLineJoin": "miter",
                "strokeMiterLimit": 4,
                "strokeWidth": 1,
                "tmpFill": null,
                "tmpStroke": null,
                "top": 178.50000000000003,
                "transformMatrix": null,
                "type": "path",
                "version": "2.0.3",
                "visible": true,
                "width": 217.5
            }, ...
        ]
    }
    extradata: {
        object_GUID: {
            initFill: "#FD9328" or ""
        }, ...
    }
    settingData: [
        {
        id: object_GUID
        picType: "vector"
        setting: {
            operationType: "engrave",
            cut: {}
            engrave: {}
            property" {}
            size: { height: 0, width: 0 }
            style: {}
            transform: {}
            }
        }, ...
    ]
}

 */

    VerifyLQXFile( root_data );
    
    GUID canvasGUID = XCSAddCanvas( doc, "default" );
    doc.canvasId = canvasGUID;
    doc.extId = "D1Pro";
    doc.version = "1.0.19";
    
    // read objects
    auto canvas_iter = root_data.find("canvasData");
    if (canvas_iter != root_data.end())
        ReadLQXCanvas( canvas_iter.value(), doc );

    // read cut data
    auto cut_iter = root_data.find("settingData");
    if (cut_iter != root_data.end())
        ReadLQXCuts( cut_iter.value(), doc );

// signedHash -- string, ignore
// machineType -- ignore
// extraData - duplicates object data initFill -- ignore

}

/******************************************************************************/
/******************************************************************************/

void ReadValidateRewrite( const string &infilePath, const string &outfilename )
{
// a stream won't work with gunzip.hh because it maintains no state between calls
// a temp file works, but is not optimal
    const uint8_t gzip_header[] = { 0x1F ,0x8B, 0x08 };
    const size_t gzip_header_length = sizeof(gzip_header);
    std::ifstream compressed_in( infilePath, ios::binary );
    std::ifstream decompressed_in;
    bool isCompressed = false;
    
    // see if data is compressed or not
    
    compressed_in.seekg( 0, ios::end );
    auto input_size = compressed_in.tellg();
    compressed_in.seekg( 0, ios::beg ); // rewind
    if (input_size > sizeof(gzip_header))
        {
        uint8_t buffer[8];
        compressed_in.read ( (char *)buffer, (std::streamsize) gzip_header_length );
        compressed_in.seekg( 0, ios::beg ); // rewind
        
        if (memcmp(gzip_header,buffer,gzip_header_length) == 0)
            isCompressed = true;
        }
    

    // if compressed, decompress data into temp
    if (isCompressed)
        {
        string decompressed_path( "decompressed.lqx");
        std::ofstream decompressed_out( decompressed_path, ios::binary );

        Deflate( [&](){ return (uint8_t)compressed_in.get(); },
                [&](unsigned char byte) { decompressed_out.put(byte); } );
        
        decompressed_out.close();   // flush to disk, free memory
        compressed_in.close();   // to prevent any further use, and free memory
        
        decompressed_in.open( decompressed_path, ios::binary );
        
        remove( decompressed_path.c_str() );       // unlink so temp disappears once we're done
        }

    std::ifstream &f = isCompressed ? decompressed_in : compressed_in;


    XCSDocument doc;
    ReadLQXFile( f, doc );
    
    fs::path temppath( infilePath );
    string pathtemp = temppath.filename().string();
    
    MatchUpIDs( doc, pathtemp, true, true );

    std::ofstream out( outfilename );
    WriteXCSFile( out, doc );
    out.close();
}

/******************************************************************************/
/******************************************************************************/

void
RegressionTestFiles()
{
    const char *testfiles[] = {
#if 1
        // change extension to _expanded.lqx.gz, use gunzip to expand
        "../../testfiles/test_circles_expanded.lqx",
        "../../testfiles/lamp3d_expanded.lqx",
        "../../testfiles/horse_expanded.lqx",
        "../../testfiles/ruler_expanded.lqx",
        "../../testfiles/panda_expanded.lqx",
        "../../testfiles/laser gradient test grid rev 3_expanded.lqx",
#endif

#if 1
        "../../testfiles/test_circles.lqx",
        "../../testfiles/lamp3d.lqx",
        "../../testfiles/horse.lqx",
        "../../testfiles/ruler.lqx",
        "../../testfiles/panda.lqx",
        "../../testfiles/laser gradient test grid rev 3.lqx",
        "../../testfiles/Shapes.lq",
#endif
    };

    for( auto &path : testfiles )
        {
        // make new filename without path or extension
        fs::path temppath( path );
        string pathtemp = temppath.stem().string() + ".xcs";
        string outfilename = string("rewrite_") + pathtemp;
        
        ReadValidateRewrite( path, outfilename );
        }
}

/******************************************************************************/
/******************************************************************************/

struct globalParameters {
    globalParameters() : do_regression(false), have_input(false),
                        outputFile( "outfile.xcs" ) {}

    bool do_regression;
    bool have_input;
    
    string inputFile;
    string outputFile;
};

/******************************************************************************/

void
print_usage(const char *progname)
{
	fprintf(stderr, "\n\n");
	fprintf(stderr, "usage:\n%s\tout_file\n", progname );
	fprintf(stderr, "\t[-i in_file.lqx]\n" );
	printf("Compiled %s %s\n", __DATE__, __TIME__ );
}

/******************************************************************************/

void parse_arguments( int argc, const char *argv[], globalParameters &params )
{
	for ( int c = 1; c < argc; ++c )
		{
		
        if ( (strcmp( argv[c], "-i" ) == 0 || strcmp( argv[c], "-I" ) == 0 )
            && c < (argc-1)  )
            {
            params.inputFile = argv[c+1];
            params.have_input = true;
            ++c;
            }
        else if ( (strcmp( argv[c], "-regression" ) == 0 || strcmp( argv[c], "-GRID" ) == 0 ) )
            {
            params.do_regression = true;
            ++c;
            }
		else if ( strcmp( argv[c], "-V" ) == 0
				|| strcmp( argv[c], "-v" ) == 0
				|| strcmp( argv[c], "-help" ) == 0
				|| strcmp( argv[c], "--help" ) == 0
				|| strcmp( argv[c], "-version" ) == 0
				|| strcmp( argv[c], "-Version" ) == 0
				)
			{
			print_usage( argv[0] );
			exit(0);
			}
		else if (argv[c][0] == '-')
			{
			// unrecognized switch
			print_usage( argv[0] );
			exit(1);
			}
        else {
            // no switch, treat as output filename
            params.outputFile = argv[c];
            }
		
		}   // for args
}

/******************************************************************************/

int
main(int argc, const char * argv[])
{

    // parse command line arguments
    globalParameters params;
	parse_arguments( argc, argv, params );


    if ( params.do_regression )
        {
        // developers need to rerun tests after any major changes
        cout << "Running regression tests\n";
        RegressionTestFiles();
        }


    if ( params.have_input )
        {
        //params.inputFile = "../../testfiles/test_circles_expanded.lqx";
        cout << "Converting " << params.inputFile << "\n";
        ReadValidateRewrite( params.inputFile,  params.outputFile );
        }

    return 0;
}

/******************************************************************************/
/******************************************************************************/
