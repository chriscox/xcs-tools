//
//  Grids.cpp
//      Create test grids
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#include <cstdint>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include "json.hpp"
#include "xcs_document.hpp"
#include "Grids.hpp"

/******************************************************************************/

using namespace std;
namespace fs = std::filesystem;

using json = nlohmann::json;
//using json = nlohmann::ordered_json;

/******************************************************************************/

void to_json( json &j, const gridDefinition &g )
{
    j = json{
            {"powers", g.powers}, {"speeds", g.speeds}, {"passes", g.passes}, {"densities", g.densities},
            {"boxSize", g.boxSize},
            {"boxSpacing", g.boxSpacing}, {"groupSpacing", g.groupSpacing},
            {"labelSize", g.labelSize}, {"labelPower", g.labelPower},
            {"labelSpeed", g.labelSpeed}, {"labelPasses", g.labelPasses},
            {"labelDensity", g.labelDensity},
            {"boxCutType", cutTypeToString( g.boxCutType ) },
            {"labelCutType", cutTypeToString( g.labelCutType ) }
            };
    
    if (g.exposureLimit != defaultExposureLimit)
        j["exposureLimit"] = g.exposureLimit;
}

/******************************************************************************/

void from_json( const json &j, gridDefinition &g )
{
    j.at("powers").get_to(g.powers);
    j.at("speeds").get_to(g.speeds);
    j.at("passes").get_to(g.passes);
    j.at("densities").get_to(g.densities);
    
    string temp;
    j.at("boxCutType").get_to(temp);
    g.boxCutType = cutStringToType( temp );
    j.at("labelCutType").get_to(temp);
    g.labelCutType = cutStringToType( temp );
    
    j.at("boxSize").get_to(g.boxSize);
    j.at("boxSpacing").get_to(g.boxSpacing);
    j.at("groupSpacing").get_to(g.groupSpacing);
    j.at("labelSize").get_to(g.labelSize);
    j.at("labelPower").get_to(g.labelPower);
    j.at("labelSpeed").get_to(g.labelSpeed);
    j.at("labelPasses").get_to(g.labelPasses);
    j.at("labelDensity").get_to(g.labelDensity);
    
    auto dataFound = j.find("exposureLimit");
    if (dataFound != j.end())
        j.at("exposureLimit").get_to(g.exposureLimit);
}

/******************************************************************************/

bool gridDefinition::isValid( bool printExplanation ) const
{
    if (!isValidCutType(boxCutType))
        {
        if (printExplanation) cout << "boxCutType is not valid\n";
        return false;
        }
    if (!isValidCutType(labelCutType))
        {
        if (printExplanation) cout << "labelCutType is not valid\n";
        return false;
        }
    if (powers.size() == 0)
        {
        if (printExplanation) cout << "powers list is empty\n";
        return false;
        }
    if (speeds.size() == 0)
        {
        if (printExplanation) cout << "speeds list is empty\n";
        return false;
        }
    if (passes.size() == 0)
        {
        if (printExplanation) cout << "passes list is empty\n";
        return false;
        }
    if (densities.size() == 0)
        {
        if (printExplanation) cout << "densities list is empty\n";
        return false;
        }
    if (labelDensity < 10 || labelDensity >= 500)
        {
        if (printExplanation) cout << "labelDensity is out of range\n";
        return false;
        }
    if (boxSize <= 0 || boxSize > 1000)
        {
        if (printExplanation) cout << "boxSize is out of range\n";
        return false;
        }
    if (boxSpacing < 0)
        {
        if (printExplanation) cout << "boxSpacing is out of range\n";
        return false;
        }
    if (groupSpacing < 0)
        {
        if (printExplanation) cout << "groupSpacing is out of range\n";
        return false;
        }
    if (labelPower <= 0 || labelPower > 100)
        {
        if (printExplanation) cout << "labelPower is out of range\n";
        return false;
        }
    if (labelSpeed <= 0 || labelSpeed > 1000)
        {
        if (printExplanation) cout << "labelSpeed is out of range\n";
        return false;
        }
    if (labelPasses <= 0 || labelPasses > 100)
        {
        if (printExplanation) cout << "labelPasses is out of range\n";
        return false;
        }
    if (exposureLimit <= 0.0 || exposureLimit > defaultExposureLimit)
        {
        if (printExplanation) cout << "exposureLimit is out of range\n";
        return false;
        }
    for ( auto &item : powers)
        {
        if (item <= 0 || item > 100)
            {
            if (printExplanation) cout << "power entry " << item << " is out of range\n";
            return false;
            }
        }
    for ( auto &item : speeds)
        {
        if (item <= 0 || item > 1000)
            {
            if (printExplanation) cout << "speed entry " << item << " is out of range\n";
            return false;
            }
        }
    for ( auto &item : passes)
        {
        if (item <= 0 || item > 100)
            {
            if (printExplanation) cout << "passes entry " << item << " is out of range\n";
            return false;
            }
        }
    for ( auto &item : densities)
        {
        if (item < 10 || item > 500)
            {
            if (printExplanation) cout << "density entry " << item << " is out of range\n";
            return false;
            }
        }
    
    return true;
}

/******************************************************************************/
/******************************************************************************/

// TODO - test if zOrder really sorts the burn order -- can we optimize movement and reduce time?
// First test, increasing -- nope, results not changed, but seems to trigger bugs in XCS or firmware causing bad positioning

// NOTE - metal business cards are 2 inches x 3 3/8 inches (54 x 86 mm)
void CreateEngraveGrid( const string &outfilename, const gridDefinition &def )
{
    size_t speedCount = def.speeds.size();
    size_t powerCount = def.powers.size();
    
    if (!def.isValid(true))
        return;
    
    // new document and canvas
    XCSDocument doc;
    GUID canvas1 = XCSAddCanvas( doc, "Grid" );
    
    // rather arbitrary starting points...
    double verticalOffset = inch * 0.5;
    double horizontalOffset = inch * 0.5;
    double verticalStart = verticalOffset;

    double boxIncrement = def.boxSize + def.boxSpacing;
    double oneSetWidth = (speedCount+1) * boxIncrement;
    double oneSetHeight = (powerCount+1) * boxIncrement;
    
    bool isCut = (def.boxCutType == xcsKnifeCut) || (def.boxCutType == xcsLaserCut);
    
    point2D boxWidthHeight( def.boxSize, def.boxSize );
    
    // iterate to create grids and labels
    for (int density : def.densities)
        {
        verticalOffset = verticalStart; // take it from the top!
        
        // overlap factor is smaller at 100, about unity at 166 lpc, larger at 200 lpc
        //      but only applies for engraving, not cutting
        // This is not a perfect model, but helps
        double overlap = 1.0;
        if (!isCut)
            overlap = double(density) / 166.0;
        
        
        for (int pass : def.passes)
            {
            GUID passGroup = NewBarelyUniqueIDString();
            
            // horizontal label with passes and density
            {
            string passText = "Speed mm/s with " + to_string( pass ) + ((pass == 1) ? " pass" : " passes");
            if (!isCut) // only show density if this is an engrave
                passText += " at " + to_string(density) + " lines/cm";
            size_t passLength = passText.length();
            point2D topLeft( horizontalOffset + 0.5*oneSetWidth + 0.7*def.boxSize - def.labelSize*point*(passLength/4.0), verticalOffset+1.0*def.boxSize-1.9*def.labelSize*point );
            GUID passLabel = XCSAddObjectText( doc, canvas1, topLeft, def.labelSize, passText, def.labelCutType, true, passGroup );
            object_data *text_data = FindObject( doc, passLabel );
            text_data->style.align = "middle";
            ChangeObjectCutSettings( doc, passLabel, def.labelCutType, def.labelPower, def.labelPasses, def.labelSpeed, "grayscale", def.labelDensity );
            }
            
            // vertical label
            {
            // offset approx half of label length vertically
            string powerText = "Power %";
            size_t powerLength = powerText.length();
            point2D topLeft( horizontalOffset+1.0*def.boxSize-1.9*def.labelSize*point, verticalOffset + 0.5*oneSetHeight + def.labelSize*point*(powerLength/2.0) );
            GUID powerLabel = XCSAddObjectText( doc, canvas1, topLeft, def.labelSize, powerText, def.labelCutType, true, passGroup );
            object_data *text_data = FindObject( doc, powerLabel );
            text_data->angle = 270;
            text_data->style.align = "middle";
            ChangeObjectCutSettings( doc, powerLabel, def.labelCutType, def.labelPower, def.labelPasses, def.labelSpeed, "grayscale", def.labelDensity );
            }
            
            // horizontal speed labels
            int x = 1;
            for (int speed : def.speeds)
                {
                double xx = x * boxIncrement;
                point2D topLeft( horizontalOffset + xx, verticalOffset+1.0*def.boxSize-0.7*def.labelSize*point );
                string speedText = to_string( speed );
                GUID speedLabel = XCSAddObjectText( doc, canvas1, topLeft, def.labelSize, speedText, def.labelCutType, true, passGroup );
                object_data *text_data = FindObject( doc, speedLabel );
                text_data->style.align = "middle";
                ChangeObjectCutSettings( doc, speedLabel, def.labelCutType, def.labelPower, def.labelPasses, def.labelSpeed, "grayscale", def.labelDensity );
                ++x;
                }   // speed
            
            // vertical power labels
            int y = 1;
            for (int power : def.powers)
                {
                double yy = y * boxIncrement;
                point2D topLeft( horizontalOffset+1.0*def.boxSize-0.7*def.labelSize*point, verticalOffset+yy+def.boxSize ); // allow for rotation of text
                string powerText = to_string( power );
                GUID powerLabel = XCSAddObjectText( doc, canvas1, topLeft, def.labelSize, powerText, def.labelCutType, true, passGroup );
                object_data *text_data = FindObject( doc, powerLabel );
                text_data->angle = 270;
                text_data->style.align = "middle";
                ChangeObjectCutSettings( doc, powerLabel, def.labelCutType, def.labelPower, def.labelPasses, def.labelSpeed, "grayscale", def.labelDensity );
                ++y;
                }
            
            // squares
            y = 1;
            for (int power: def.powers)
                {
                double yy = y * boxIncrement;
                
                x = 1;
                for (int speed : def.speeds)
                    {
                    double exposure = (overlap * pass * power) / double(speed);
                    
                    if (exposure <= def.exposureLimit)
                        {
                        double xx = x * boxIncrement;
                        point2D topLeft( horizontalOffset + xx, verticalOffset + yy );
                        GUID rect1 = XCSAddObjectRect( doc, canvas1, topLeft, boxWidthHeight, def.boxCutType, true, passGroup );
                        ChangeObjectCutSettings( doc, rect1, def.boxCutType, power, pass, speed, "grayscale", density );
                        }
                    else
                        {
                        // Should I add a symbol indicating this was intentionally left out?
                        // or just not include it and let the job run faster?     Faster does seem to be the better option.
                        }
                    
                    ++x;
                    }   // speed
                
                ++y;
                }   // power
            
            // offset to next group of passes
            verticalOffset += oneSetHeight + def.groupSpacing;
            }   // passes
            
        // offset to next group of densities
        horizontalOffset += oneSetWidth + def.groupSpacing;
        }   // densities
    
    
//    SortObjectsZOrder( doc, false );  // seems to trigger bugs in XCS or the firmware

    // verify the document
    MatchUpIDs( doc, outfilename, true, true );
    
    // and save
    std::ofstream out( outfilename );
    WriteXCSFile( out, doc );
    out.close();
}

/******************************************************************************/
/******************************************************************************/
