//
//  xcs_document.cpp
//  xcs_tools -- to read and write XCS files for xTool Creative Space
//
//  Created by Chris Cox on 11/14/22.
//  Copyright 2022-2023, by Chris Cox.
//

#include <cstdint>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <filesystem>
#include <chrono>
#include <algorithm>
#include "json.hpp"
#include "xcs_document.hpp"

/******************************************************************************/

using namespace std;
namespace fs = std::filesystem;

/******************************************************************************/
/******************************************************************************/

/* based on crand64 from benchmark_algorithms.h */
constexpr uint64_t hash64( uint64_t x )
{
    const uint64_t a = 6364136223846793005ULL;
    const uint64_t c = 1442695040888963407ULL;
    uint64_t temp = (x * a) + c;
    
    // without bit mixing the result is really bad, shows lots of periodicity
    temp = (temp >> 20) ^ (temp << 23) ^ temp;        // looks better
    return temp;
}

/******************************************************************************/

//  aka HackUID aka NotUID aka JustGoodEnoughUID
GUID NewBarelyUniqueIDString()
{
    // https://en.wikipedia.org/wiki/Universally_unique_identifier
    
    const uint8_t hex_encode[] = "0123456789abcdef";
    
    // internal 64 bit value for session
    // NOTE: std::chrono is obviously the result of an obfuscated C++ contest
    static uint64_t session = hash64( chrono::time_point_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now()).time_since_epoch().count() );

    // internal incremented number for "random" bits
    static uint64_t counter = hash64( 0x1BADBABEFEEDBEEFULL );
    ++counter;
    

    // assemble bits into string
    uint8_t format[] = "xxxxxxxx-xxxx-4xxx-bxxx-xxxxxxxxxxxx";
    uint8_t *session_bytes = (uint8_t *)(&session);     // byte order doesn't matter here, they're semi-random bits
    
    // hypens at 8, 13, 18, 23
    // starts at 0, 9, 15, 20, 24
    // end at 35
    // xxxxxxxx
    for (int i = 0; i < 4; ++i)
        {
        format[2*i+0] = hex_encode[ session_bytes[i] >> 4 ];
        format[2*i+1] = hex_encode[ session_bytes[i] & 0xf ];
        }
    
    // -xxxx
    for (int i = 0; i < 2; ++i)
        {
        format[9+2*i+0] = hex_encode[ session_bytes[4+i] >> 4 ];
        format[9+2*i+1] = hex_encode[ session_bytes[4+i] & 0xf ];
        }
    
    // -4xxx
    format[15+0] = hex_encode[ session_bytes[6] >> 4 ];
    format[15+1] = hex_encode[ session_bytes[6] & 0xf ];
    format[15+2] = hex_encode[ session_bytes[7] >> 4 ];
    
    // -bxxx
    // must be careful to get little end of the counter, can't rely on byte order
    format[20+0] = hex_encode[ (counter >> (7*8)) & 0xf  ];
    format[20+1] = hex_encode[ (counter >> (6*8+4)) & 0xf  ];
    format[20+2] = hex_encode[ (counter >> (6*8)) & 0xf ];

    // -xxxxxxxxxxxx
    for (int i = 0; i < 6; ++i)
        {
        format[35-(2*i)-0] = hex_encode[ (counter >> (i*8)) & 0xf ];
        format[35-(2*i)-1] = hex_encode[ (counter >> (i*8+4)) & 0xf ];
        }
    
    string result( (const char *)format );
    return result;
}


/******************************************************************************/
/******************************************************************************/

// 3 bytes of input -> 4 bytes of output
size_t base64Encode( const uint8_t *input, size_t input_length, uint8_t *output )
{
    static uint8_t dtable[256] = {0};
    
    uint8_t *output_base = output;

    // init and cache our table
    if (dtable[0] == 0)
        {
        memset(dtable,0,256);
        for( int i = 0; i < 9; ++i )
            {
            dtable[i] = 'A'+i;
            dtable[i+9] = 'J'+i;
            dtable[26+i] = 'a'+i;
            dtable[26+i+9] = 'j'+i;
            }
        for( int i = 0; i < 8; ++i )
            {
            dtable[i+18] = 'S'+i;
            dtable[26+i+18] = 's'+i;
            }
        for( int i = 0; i < 10; ++i )
            dtable[52+i] = '0'+i;
        
        dtable[62] = '+';
        dtable[63] = '/';
        }


    const uint8_t *lastInput = input + input_length;
    bool hitEOF = false;

    do
        {
        uint8_t igroup[3];

        igroup[0] = igroup[1] = igroup[2] = 0;
        
        int n;
        for( n = 0; n < 3; ++n )
            {
            if (input >= lastInput)
                {
                hitEOF = true;
                break;
                }
            igroup[n] = *input++;
            }
        
        if ( n > 0)
            {
            uint8_t ogroup[4];
            
            ogroup[0] = dtable[ igroup[0] >> 2 ];
            ogroup[1] = dtable[ ((igroup[0]&3)<<4) | (igroup[1]>>4) ];
            ogroup[2] = dtable[ ((igroup[1]&0xF)<<2) | (igroup[2]>>6) ];
            ogroup[3] = dtable[ igroup[2] & 0x3F ];

            if (n<3)
                ogroup[3]= '=';
            
            if (n<2)
                ogroup[2]= '=';
            
            for(int i = 0; i < 4; ++i )
                *output++ = ogroup[i];
            }
        } while ( !hitEOF );
    
    return size_t(output - output_base);

}

/******************************************************************************/

// 4 bytes of input -> 3 bytes of output
// newlines and returns must be removed first
size_t base64Decode( const uint8_t *input, size_t input_length, uint8_t *output )
{
    int i;

    static uint8_t dtable[256] = {0};
    
    uint8_t *output_base = output;

    // init and cache our table
    if (dtable[0] == 0)
        {
        memset(dtable,0x80,256);
        for (i= 'A';i<='I';i++)
            dtable[i] = 0+(i-'A');
        for (i= 'J';i<='R';i++)
            dtable[i] = 9+(i-'J');
        for (i= 'S';i<='Z';i++)
            dtable[i] = 18+(i-'S');
        for (i= 'a';i<='i';i++)
            dtable[i] = 26+(i-'a');
        for (i= 'j';i<='r';i++)
            dtable[i] = 35+(i-'j');
        for (i= 's';i<='z';i++)
            dtable[i] = 44+(i-'s');
        for (i= '0';i<='9';i++)
            dtable[i] = 52+(i-'0');
        dtable['+'] = 62;
        dtable['/'] = 63;
        dtable['='] = 0;
        }

    const uint8_t *lastInput = input + input_length;

    do
        {
        uint8_t a[4],b[4],o[3];

        for( i = 0; i < 4; ++i )
            {
            if (input >= lastInput)
                {
                if( i > 0 )
                    cerr << "Base64 input incomplete.\n";
                break;
                }
            
            uint8_t c = *input++;
            
            if ( (dtable[c] &0x80) != 0 )
                {
                cerr << "Illegal character '" << c << " in base64 input.\n";
                return 0;
                }
            
            a[i] = c;
            b[i] = dtable[c];
            }
        
        if (i > 0)
            {
            o[0] = (b[0]<<2) | (b[1]>>4);
            o[1] = (b[1]<<4) | (b[2]>>2);
            o[2] = (b[2]<<6) | b[3];
            i = (a[2]=='=') ? 1 : ( (a[3]=='=')? 2 : 3 );
            
            for(int k = 0; k < i; ++k )
                *output++ = o[k];
            }
        
        } while (i == 3);
    
    return size_t(output - output_base);

}

/******************************************************************************/

void UnitTestBase64Functions(void)
{
    const size_t testMaxSize = 4000;
    const size_t size64 = 4*((testMaxSize+2)/3);
    
    uint8_t testInput[testMaxSize+2];
    uint8_t test64Output[size64];
    uint8_t test64Decode[testMaxSize+2];
    
    memset(testInput,0,testMaxSize+2);
    
    for (int len = 0; len < testMaxSize; ++len )
        {
        for (int j = 0; j < len; ++j)
            testInput[j] = j & 0xFF;
        
        memset(test64Output,0,size64);
        memset(test64Decode,0,testMaxSize+2);
        
        auto encLen = base64Encode( testInput, len, test64Output );
        auto decLen = base64Decode( test64Output, encLen, test64Decode );
        
        assert( encLen == 4*((len+2)/3) );
        assert( decLen == len );
        assert( memcmp( testInput,test64Decode, len+2 ) == 0 );
        }


    const uint8_t encoded_string1[] = "ewogICAgImJveEN1dFR5cGUiOiAiRklMTF9WRUNUT1JfRU5HUkFWSU5HIiwKICAgICJib3hEZW5zaXR5IjogMTAwLAogICAgImJveFNpemUiOiA1LjAsCiAgICAiYm94U3BhY2luZyI6IDEuNSwKICAgICJncm91cFNwYWNpbmciOiAxNS4wLAogICAgImxhYmVsQ3V0VHlwZSI6ICJGSUxMX1ZFQ1RPUl9FTkdSQVZJTkciLAogICAgImxhYmVsRGVuc2l0eSI6IDEwMCwKICAgICJsYWJlbFBhc3NlcyI6IDEsCiAgICAibGFiZWxQb3dlciI6IDgwLAogICAgImxhYmVsU2l6ZSI6IDEwLjAsCiAgICAibGFiZWxTcGVlZCI6IDYwLAogICAgInBhc3NlcyI6IFsKICAgICAgICAxLAogICAgICAgIDIsCiAgICAgICAgMwogICAgXSwKICAgICJwb3dlcnMiOiBbCiAgICAgICAgMTAsCiAgICAgICAgMjAsCiAgICAgICAgMzAsCiAgICAgICAgNDAsCiAgICAgICAgNTAsCiAgICAgICAgNjAsCiAgICAgICAgNzAsCiAgICAgICAgODAsCiAgICAgICAgOTAsCiAgICAgICAgMTAwCiAgICBdLAogICAgInNwZWVkcyI6IFsKICAgICAgICAyMCwKICAgICAgICA0MCwKICAgICAgICA2MCwKICAgICAgICA4MCwKICAgICAgICAxMDAsCiAgICAgICAgMTIwLAogICAgICAgIDE0MCwKICAgICAgICAxNjAsCiAgICAgICAgMTgwLAogICAgICAgIDIwMAogICAgXQp9Cg==";
    const size_t string1_len = 643;
    
    const size_t encoded_len = strlen( (char *)encoded_string1 );
    assert( encoded_len < testMaxSize );
    assert( string1_len < testMaxSize );
    auto decLen1 = base64Decode( encoded_string1, encoded_len, test64Decode );
    assert( decLen1 == string1_len );
    test64Decode[decLen1] = 0;  // add NULL termination to string
    json temp = json::parse(test64Decode);  // make sure the string makes sense
    
    
    const uint8_t encoded_string2[] = "TUlUIExpY2Vuc2UKCkNvcHlyaWdodCAoYykgMjAyMiBDaHJpcyBDb3gKClBlcm1pc3Npb24gaXMg"
                                    "aGVyZWJ5IGdyYW50ZWQsIGZyZWUgb2YgY2hhcmdlLCB0byBhbnkgcGVyc29uIG9idGFpbmluZyBh"
                                    "IGNvcHkKb2YgdGhpcyBzb2Z0d2FyZSBhbmQgYXNzb2NpYXRlZCBkb2N1bWVudGF0aW9uIGZpbGVz"
                                    "ICh0aGUgIlNvZnR3YXJlIiksIHRvIGRlYWwKaW4gdGhlIFNvZnR3YXJlIHdpdGhvdXQgcmVzdHJp"
                                    "Y3Rpb24sIGluY2x1ZGluZyB3aXRob3V0IGxpbWl0YXRpb24gdGhlIHJpZ2h0cwp0byB1c2UsIGNv"
                                    "cHksIG1vZGlmeSwgbWVyZ2UsIHB1Ymxpc2gsIGRpc3RyaWJ1dGUsIHN1YmxpY2Vuc2UsIGFuZC9v"
                                    "ciBzZWxsCmNvcGllcyBvZiB0aGUgU29mdHdhcmUsIGFuZCB0byBwZXJtaXQgcGVyc29ucyB0byB3"
                                    "aG9tIHRoZSBTb2Z0d2FyZSBpcwpmdXJuaXNoZWQgdG8gZG8gc28sIHN1YmplY3QgdG8gdGhlIGZv"
                                    "bGxvd2luZyBjb25kaXRpb25zOgoKVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMg"
                                    "cGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWQgaW4gYWxsCmNvcGllcyBvciBzdWJz"
                                    "dGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuCgpUSEUgU09GVFdBUkUgSVMgUFJPVklE"
                                    "RUQgIkFTIElTIiwgV0lUSE9VVCBXQVJSQU5UWSBPRiBBTlkgS0lORCwgRVhQUkVTUyBPUgpJTVBM"
                                    "SUVELCBJTkNMVURJTkcgQlVUIE5PVCBMSU1JVEVEIFRPIFRIRSBXQVJSQU5USUVTIE9GIE1FUkNI"
                                    "QU5UQUJJTElUWSwKRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UgQU5EIE5PTklORlJJ"
                                    "TkdFTUVOVC4gSU4gTk8gRVZFTlQgU0hBTEwgVEhFCkFVVEhPUlMgT1IgQ09QWVJJR0hUIEhPTERF"
                                    "UlMgQkUgTElBQkxFIEZPUiBBTlkgQ0xBSU0sIERBTUFHRVMgT1IgT1RIRVIKTElBQklMSVRZLCBX"
                                    "SEVUSEVSIElOIEFOIEFDVElPTiBPRiBDT05UUkFDVCwgVE9SVCBPUiBPVEhFUldJU0UsIEFSSVNJ"
                                    "TkcgRlJPTSwKT1VUIE9GIE9SIElOIENPTk5FQ1RJT04gV0lUSCBUSEUgU09GVFdBUkUgT1IgVEhF"
                                    "IFVTRSBPUiBPVEhFUiBERUFMSU5HUyBJTiBUSEUKU09GVFdBUkUuCg==";
    const size_t string2_len = 1066;
    
    const size_t encoded_len2 = strlen( (char *)encoded_string2 );
    assert( encoded_len2 < testMaxSize );
    assert( string2_len < testMaxSize );
    auto decLen2 = base64Decode( encoded_string2, encoded_len2, test64Decode );
    assert( decLen2 == string2_len );

}

/******************************************************************************/
/******************************************************************************/

bool isValidCutType( const string &cut )
{
    if (cut == "FILL_VECTOR_ENGRAVING")
        return true;
    if (cut == "KNIFE_CUTTING")
        return true;
    if (cut == "VECTOR_CUTTING")
        return true;
    if (cut == "VECTOR_ENGRAVING")
        return true;
    if (cut == "BITMAP_ENGRAVING")
        return true;
    
    return false;
}

/******************************************************************************/

bool isValidCutType( const xcsCutType cut )
{
    switch (cut)
        {
        case xcsFillEngraving:
        case xcsKnifeCut:
        case xcsLaserCut:
        case xcsVectorEngrave:
        case xcsBitmapEngraving:
            return true;
        }
    
    return false;
}

/******************************************************************************/

string cutTypeToString( const xcsCutType cut )
{
    switch (cut)
        {
        case xcsFillEngraving:
            return "FILL_VECTOR_ENGRAVING";
            break;
        case xcsKnifeCut:
            return "KNIFE_CUTTING";
            break;
        case xcsLaserCut:
            return "VECTOR_CUTTING";
            break;
        case xcsVectorEngrave:
            return "VECTOR_ENGRAVING";
            break;
        case xcsBitmapEngraving:
            return "BITMAP_ENGRAVING";
            break;
        default:
            assert(false);  // how did we get here!?
            break;
        }
    
    return "";
}

/******************************************************************************/

xcsCutType cutStringToType( const string &cut )
{
    if (cut == "FILL_VECTOR_ENGRAVING")
        return xcsFillEngraving;
    if (cut == "KNIFE_CUTTING")
        return xcsKnifeCut;
    if (cut == "VECTOR_CUTTING")
        return xcsLaserCut;
    if (cut == "VECTOR_ENGRAVING")
        return xcsVectorEngrave;
    if (cut == "BITMAP_ENGRAVING")
        return xcsBitmapEngraving;
    
    assert(false);
    return xcsLaserCut;
}

/******************************************************************************/
/******************************************************************************/

typedef unordered_set<string> keyList;

void ValidateKeys( json &input, const keyList &required_keys, const keyList &optional_keys, const string &description )
{
    keyList leftovers( required_keys );

    for ( auto& [key, value] : input.items())
        {
        if (required_keys.count(key))
            {
            leftovers.erase(key);
            }
        else
            {
            if (!optional_keys.count(key))
                {
                // this is an unknown key:  document it
                cout << "UNKNOWN key in " << description << " ; " << key << " : " << value.dump(4) << '\n';
                }
            }
        }
    
    for ( auto &item : leftovers)
        {
        // oops, required key is missing... or not really required.
        cout << "KEY NOT FOUND in " << description << " ; " << item << '\n';
        }
}

/******************************************************************************/

void VerifyXCSFile( json &root_data )
{
    keyList requiredDocumentKeys = { "canvas", "canvasId", "extId", "version"};
    keyList optionalDocumentKeys = { "device", "created", "meta", "modify", "ua" };
    ValidateKeys( root_data, requiredDocumentKeys, optionalDocumentKeys, "XCSfile" );
}

/******************************************************************************/

void VerifyMeta( json &meta )
{
    keyList requiredMetaKeys = { "date", "ua", "version"};
    keyList optionalMetaKeys = { };
    ValidateKeys( meta, requiredMetaKeys, optionalMetaKeys, "file meta" );
}

/******************************************************************************/

void VerifyDevices( json &device )
{
    keyList requiredDeviceKeys = { "power", "materialList", "id", "data" };
    keyList optionalDeviceKeys = { "materialTypeList" };
    ValidateKeys( device, requiredDeviceKeys, optionalDeviceKeys, "device" );
}

/******************************************************************************/

void VerifyCanvas( json &in )
{
    keyList requiredCanvasKeys = { "displays", "id", "title" };
    keyList optionalCanvasKeys = { "layerData" };   // added 1.3.21
    ValidateKeys( in, requiredCanvasKeys, optionalCanvasKeys, "canvas" );
}

/******************************************************************************/

void VerifyObject( json &object )
{
    keyList requiredObjectKeys = { "id", "angle", "height", "width", "x", "y", "isClosePath", "isFill",
                            "lockRatio", "offsetX", "offsetY", "localSkew", "pivot", "scale", "skew",
                            "type", "zOrder" };
    keyList optionalObjectKeys = { "endPoint", "dPath", "graphicX", "graphicY", "points", "resolution",
                                    "style", "text", "controlPoints", "groupTag", "fillColor",
                                    "lineColor", "filterList", "grayValue", "sharpness", "base64", "sourceId",
                                    "charJSONs", "fontData", "colorInverted",
                                    "layerColor", "layerTag", "fillRule",        // added 1.3.21
                                    "originColor", "radius", "maxRadius",       // added 1.4.11
    };
    ValidateKeys( object, requiredObjectKeys, optionalObjectKeys, "object" );
}

/******************************************************************************/

void VerifyLayerData( json &object )
{
    keyList requiredKeys = { "name" };
    keyList optionalKeys = { "order" };
    ValidateKeys( object, requiredKeys, optionalKeys, "layer data" );
}

/******************************************************************************/

void VerifyFontInfo( json &object )
{
    keyList requiredKeys = { "lineHeight", "unitsPerEm" };
    keyList optionalKeys = { "ascent", "capHeight", "descent", "lineGap", "xHeight" };  // 1.4.11
    ValidateKeys( object, requiredKeys, optionalKeys, "font info" );
}

/******************************************************************************/

void VerifyFontData( json &object )
{
    keyList requiredKeys = { "fontInfo", "glyphData" };
    keyList optionalKeys = { };
    ValidateKeys( object, requiredKeys, optionalKeys, "font data" );
}

/******************************************************************************/

void VerifyGlyphData( json &object )
{
    keyList requiredKeys = { "advanceHeight", "advanceWidth", "dPath" };
    keyList optionalKeys = { "bbox", "leftBearing", "topBearing" };
    ValidateKeys( object, requiredKeys, optionalKeys, "glyph data" );
}

/******************************************************************************/

void VerifyTextStyle( json &object )
{
    keyList requiredStyleKeys = { "align", "fontFamily", "fontSize", "fontSubfamily",
            "leading", "letterSpacing" };
    keyList optionalStyleKeys = { "fontSource", "curveX", "curveY" };
    ValidateKeys( object, requiredStyleKeys, optionalStyleKeys, "type style" );
}

/******************************************************************************/

void VerifyMaterial( json &object )
{
    keyList requiredMaterialKeys = { "categoryId", "categoryName", "createdAt", "createdBy", "diameter",
                                     "helpUrl", "id", "image", "isPreset", "machineParas", "name",
                                     "saleStatus", "saleUrl", "skus", "status", "thickness", "typeId",
                                     "typeName", "updatedAt", "updatedBy" };
    keyList optionalMaterialKeys = { "deletedAt" };
    ValidateKeys( object, requiredMaterialKeys, optionalMaterialKeys, "materials" );
}

/******************************************************************************/

void VerifySku( json &object )
{
    keyList requiredSkuKeys = { "code", "id", "key", "name" };
    keyList optionalSkuKeys = { };
    ValidateKeys( object, requiredSkuKeys, optionalSkuKeys, "sku" );
}

/******************************************************************************/

void VerifyMachineParas( json &object )
{
    keyList requiredParasKeys = { "bitmapMode", "bitmapModeId", "bladeType", "bladeTypeId", "createdAt",
                                    "createdBy", "cutPressure", "density", "densityId", "deviceModeId",
                                    "id", "laserPowerId", "materialId", "power", "processingTypeId", "repeat",
                                    "speed", "updatedAt", "updatedBy", "deviceMode", "laserPower", "processingType" };
    keyList optionalParasKeys = { "deletedAt" };
    ValidateKeys( object, requiredParasKeys, optionalParasKeys, "machineParas" );
}

/******************************************************************************/

void VerifyCodeName( json &object )
{
    keyList requiredCodeNameKeys = { "code", "id", "name" };
    keyList optionalCodeNameKeys = { };
    ValidateKeys( object, requiredCodeNameKeys, optionalCodeNameKeys, "code_name" );
}

/******************************************************************************/

void VerifyCanvasCut( json &object )
{
    keyList requiredCanvasCutKeys = { "data", "displays", "mode" };
    keyList optionalCanvasCutKeys = { "needUpdateCanvas" };
    ValidateKeys( object, requiredCanvasCutKeys, optionalCanvasCutKeys, "canvas cut" );
}

/******************************************************************************/

void VerifyLaserPlane( json &object )
{
    keyList requiredSurfaceKeys = { "material" };
    keyList optionalSurfaceKeys = { "thickness", "cushion", "diameter", "perimeter",
                        "focalLength", "rotaryAttachmentType", "lightSourceMode"
    };
    ValidateKeys( object, requiredSurfaceKeys, optionalSurfaceKeys, "surface" );
}

/******************************************************************************/

void VerifyDisplays( json &object )
{
    keyList requiredDisplaysKeys = { "processingType", "type", "data" };
    keyList optionalDisplaysKeys = { "isFill", "processIgnore" };
    ValidateKeys(object, requiredDisplaysKeys, optionalDisplaysKeys, "displays" );
}

/******************************************************************************/

void VerifyObjectCut( json &object )
{
    keyList requiredObjectCutKeys = { "materialType", "parameter" };
    keyList optionalObjectCutKeys = { "processIgnore" };
    ValidateKeys( object, requiredObjectCutKeys, optionalObjectCutKeys, "object cut data" );
}

/******************************************************************************/

void VerifyCutParameters( json &object )
{
    keyList requiredCutParamKeys = { "repeat", "speed" };
    keyList optionalCutParamKeys = { "bitmapMode", "density", "power", "cutPressure", "materialType",
                                    "bitmapScanMode", "processingLightSource",
                                    "bitmapEngraveMode", "dotDuration", "breakPointCount", "breakPointCut", "breakPointPower", "breakPointSize", "enableBreakPoint", "dpi", "powerMinMaxRange"
    };
    ValidateKeys( object, requiredCutParamKeys, optionalCutParamKeys, "cut parameters" );
}

/******************************************************************************/
/******************************************************************************/

void DisplayIfFound( json &input, const char *key )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        cout << dataFound.key() << " : " << dataFound.value().dump(4) << '\n';
        }
}

/******************************************************************************/

void DisplayInnerItemsKeyValue( json &input )
{
    for ( auto& [key, value] : input.items())
        {
        cout << key << " : " << value.dump(4) << '\n';
        }
}

/******************************************************************************/

void DisplayInnerItemsKeyOnly( json &input )
{
    for ( auto& [key, value] : input.items())
        {
        cout << key << '\n';
        }
}

/******************************************************************************/

void DisplayPoint2D( json &input, const char *key )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto xx = dataFound.value().find("x");
        auto yy = dataFound.value().find("y");
        cout << key << " ( " << xx.value() << ", " << yy.value() << " )\n";
        }
}

/******************************************************************************/
/******************************************************************************/

void ReadGUID( json &input, const char *key, GUID &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        if (dataFound.value().type() == json::value_t::null)
            return;
        assert( dataFound.value().type() == json::value_t::string );
        result = dataFound.value();
        }
}

/******************************************************************************/

void ReadString( json &input, const char *key, string &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto type = dataFound.value().type();
        if (type == json::value_t::null)
            return;
        assert( type == json::value_t::string );
        result = dataFound.value();
        }
}

/******************************************************************************/

// handle some poorly written files
void ReadIntOrString( json &input, const char *key, int &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto type = dataFound.value().type();
        if (type == json::value_t::null)
            return;
        if (type == json::value_t::string )
            {
            string tempStr = dataFound.value();
            result = stoi( tempStr.c_str() );
            }
        else
            {
            assert( type == json::value_t::number_unsigned || type == json::value_t::number_integer );
            result = dataFound.value();
            }
        }
}

/******************************************************************************/

void ReadLocalizedText( json &object, localized_list &names )
{
    for ( auto& [key2, value2] : object.items())
        {
        localized_text temp;
        temp.locale = key2;
        temp.text = value2;
        names.insert( { key2, temp } );
        }
}

/******************************************************************************/

void ReadInt( json &input, const char *key, int &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto type = dataFound.value().type();
        if (type == json::value_t::null )
            {
            result = -1;
            return;
            }
        assert( type == json::value_t::number_unsigned || type == json::value_t::number_integer );
        result = dataFound.value();
        }
}

/******************************************************************************/

void ReadDouble( json &input, const char *key, double &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        auto type = dataFound.value().type();
        if (type == json::value_t::null )
            return;
        assert( type == json::value_t::number_float || type == json::value_t::number_unsigned || type == json::value_t::number_integer );
        result = dataFound.value();
        }
}

/******************************************************************************/

void ReadBool( json &input, const char *key, bool &result )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        if (dataFound.value().type() == json::value_t::null )
            return;
        assert( dataFound.value().type() == json::value_t::boolean );
        result = (dataFound.value() == true);
        }
}

/******************************************************************************/

template<typename T>
void ReadPoint2D( T &input, point2D &pt )
{
    ReadDouble( input.value(), "x", pt.x );
    ReadDouble( input.value(), "y", pt.y );
}

/******************************************************************************/

void ReadPoint2D( json &input, const char *key, point2D &pt )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        ReadPoint2D( dataFound, pt );
}

/******************************************************************************/

void ReadPointList( json &object, const char *key, vector<point2D> &pts )
{
    // array of points, may be empty
    auto pointlist = object.find(key);
    if (pointlist != object.end())
        {
        size_t count = pointlist.value().size();
        pts.reserve(count);
        
        // array of point2D
        for ( auto& [key2, value2] : pointlist.value().items())
            {
            point2D temp;
            ReadDouble( value2, "x", temp.x );
            ReadDouble( value2, "y", temp.y );
            pts.push_back(temp);
            }
        }
}

/******************************************************************************/

void ReadDoubleList( json &object, const char *key, vector<double> &pts )
{
    // array of doubles, may be empty
    auto pointlist = object.find(key);
    if (pointlist != object.end())
        {
        size_t count = pointlist.value().size();
        pts.reserve(count);
        
        // array of doubles
        for ( auto& [key2, value2] : pointlist.value().items())
            {
            pts.push_back(value2);
            }
        }
}

/******************************************************************************/

void ReadIntList( json &object, const char *key, vector<int> &list )
{
    // array of values, may be empty
    auto value_list = object.find(key);
    if (value_list != object.end())
        {
        size_t count = value_list.value().size();
        list.reserve(count);
        
        // array of object cut settings
        for ( auto& [key2, value2] : value_list.value().items())
            {
            int val = value2;
            
            list.push_back(val);
            }
        }
}

/******************************************************************************/

void ReadStringList( json &object, const char *key, vector<string> &list )
{
    // array of values, may be empty
    auto value_list = object.find(key);
    if (value_list != object.end())
        {
        size_t count = value_list.value().size();
        list.reserve(count);
        
        // array of object cut settings
        for ( auto& [key2, value2] : value_list.value().items())
            {
            string val = value2;
            list.push_back(val);
            }
        }
}

/******************************************************************************/

void ReadCutParameters( json &object, cut_parameter_struct &params )
{
    // currently always one object, matching name of materialType, but keep the code generic...
    for ( auto& [key, value] : object.items())
        {
        VerifyCutParameters( value );
        
        params.name = key;
        ReadInt( value, "repeat", params.repeat );
        ReadInt( value, "speed", params.speed );
        ReadString( value, "bitmapMode" , params.bitmapMode );
        ReadInt( value, "density", params.density );
        ReadInt( value, "power", params.power );
        ReadInt( value, "cutPressure", params.cutPressure );
        ReadString( value, "materialType" , params.materialType );
        ReadString( value, "bitmapScanMode" , params.bitmapScanMode );      // 1.3.21
        ReadString( value, "processingLightSource" , params.processingLightSource );    // 1.3.21
        ReadString( value, "bitmapEngraveMode" , params.bitmapEngraveMode );    // 1.3.21
        
        // the breakPoint data is very poorly written, should have been an object, with default empty
        ReadInt( value, "dotDuration", params.dotDuration );    // 1.3.21
        ReadInt( value, "breakPointCount", params.breakPointCount );    // 1.3.21
        ReadBool( value, "breakPointCut", params.breakPointCut );    // 1.3.21
        ReadInt( value, "breakPointPower", params.breakPointPower );    // 1.3.21
        ReadDouble( value, "breakPointSize", params.breakPointSize );    // 1.3.21
        ReadBool( value, "enableBreakPoint", params.enableBreakPoint );    // 1.3.21
        
        ReadInt( value, "dpi", params.dpi );    // 1.3.21
        
        ReadIntList( value, "powerMinMaxRange", params.powerMinMaxRange );
        
        }
}

/******************************************************************************/

void ReadObjectCut( json &object, object_cut_list &cut_list )
{
    // currently always one object, matching name of processingType, but keep the code generic...
    for ( auto& [key, value] : object.items())
        {
        VerifyObjectCut( value );
        
        cut_types_struct temp;
        temp.name = key;

        ReadString( value, "materialType", temp.materialType );
        ReadBool( value, "processIgnore", temp.processIgnore );
        
        auto parameters = value.find("parameter");
        if (parameters != value.end())
            ReadCutParameters( parameters.value(), temp.parameters );
        
        cut_list.insert( { temp.name, temp } );
        }
}

/******************************************************************************/

void ReadDisplays( json &input, displayList &display_data )
{
    keyList requiredListKeys = { "dataType", "value"};
    keyList optionalListKeys = { };
    ValidateKeys( input, requiredListKeys, optionalListKeys, "displays list" );
    
    // dataType == Map  -- this is just stupid, since the JSON basic type IS a map!
    auto dataType = input.find("dataType");
    assert( dataType.value() == string("Map") );

    // value == array of item cut parameters
    auto object_cut_list = input.find("value");
    if (object_cut_list != input.end())
        {
        // array of object cut settings
        for ( auto& [key2, value2] : object_cut_list.value().items())
            {
            assert( value2.size() == 2 );

            VerifyDisplays( value2[1] );
            
            // each item is an array, with 2 entries for GUID then data
            // keys are just numbers
            displays_struct temp;
            assert( value2[0].type() == json::value_t::string );
            temp.id = value2[0];  // GUID matching object

            // data - struct of cut settings
            auto cut_params = value2[1].find("data");
            if (cut_params != value2[1].end())
                ReadObjectCut( cut_params.value(), temp.cut_settings );
            
            ReadBool( value2[1], "isFill", temp.isFill );
            ReadString( value2[1], "processingType", temp.processingType );
            ReadString( value2[1], "type", temp.type );
            ReadBool( value2[1], "processIgnore", temp.processIgnore );// 1.3.21
            
            display_data.insert( { temp.id, temp } );
            }
        }
}

/******************************************************************************/

void ReadSurface( json &object, laser_plane_struct &surface )
{
    // currently always one object, matching name of processingType, but keep the code generic...
    for ( auto& [key, value] : object.items())
        {
        VerifyLaserPlane( value );
        
        surface.type = key;

        ReadInt( value, "material", surface.material );
        ReadDouble( value, "thickness", surface.thickness );
        
        ReadInt( value, "cushion", surface.cushion );
        ReadDouble( value, "diameter", surface.diameter );
        ReadDouble( value, "perimeter", surface.perimeter );
        ReadDouble( value, "focalLength", surface.focalLength );
        ReadString( value, "rotaryAttachmentType", surface.rotaryAttachmentType );
        ReadString( value, "lightSourceMode", surface.lightSourceMode );
        }
}

/******************************************************************************/

void ReadCuts( json &object, cutList &cut_data )
{
    // this is an array, so keys are just numbers -- also stupid, since each includes a GUID key inside
    // each item is a canvas matching list of item cut parameters
    //  [ CANVAS_GUID, {"data" : {} } ]
    for ( auto& [key, value] : object.items())
        {
        assert( value.size() == 2 );
        
        VerifyCanvasCut( value[1] );
        
        cut_struct temp;
        
        assert( value[0].type() == json::value_t::string );
        temp.id = value[0];  // GUID matching canvas

        // parameters for surface (plane, roller, extension, etc.)  "LASER_PLANE"
        auto surface_data = value[1].find("data");
        if (surface_data != value[1].end())
            ReadSurface( surface_data.value(), temp.laser_plane );
        
        // another mis-stored map with object cut parameters
        auto display_data = value[1].find("displays");
        if (display_data != value[1].end())
            ReadDisplays( display_data.value(), temp.displays );

        ReadString( value[1], "mode", temp.mode );
        ReadBool( value[1], "needUpdateCanvas", temp.needUpdateCanvas );
       
        cut_data.insert( { temp.id, temp } );
        }
}

/******************************************************************************/

void ReadSkuList( json &object, sku_list &skus )
{
    // this is an array, so keys are just numbers
    // currently always a single item, but better safe than sorry
    for ( auto& [key, value] : object.items())
        {
        VerifySku( value );
        
        sku_struct temp;
        
        ReadString( value, "code", temp.code );
        ReadInt( value, "id", temp.id );
        ReadString( value, "key", temp.key );
        ReadString( value, "name", temp.name );
       
        skus.push_back( temp );
        }
}

/******************************************************************************/

void ReadCodeName( json &input, const char *key, code_name_struct &out )
{
    auto dataFound = input.find(key);
    if (dataFound != input.end())
        {
        if (dataFound.value().type() == json::value_t::null)
            return;
        
        VerifyCodeName( dataFound.value() );
        
        ReadString( dataFound.value(), "code", out.code );
        ReadInt( dataFound.value(), "id", out.id );
        ReadString( dataFound.value(), "name", out.name );
        }
}

/******************************************************************************/

// array of structs
void ReadMachineParas( json &object, machineParasList &paras )
{
    // aray, so keys are just numbers
    for ( auto& [key, value] : object.items())
        {
        
        VerifyMachineParas( value );
        
        machineParas_struct temp;
        
        ReadInt( value, "bitmapModeId", temp.bitmapModeId );
        ReadString( value, "bladeType", temp.bladeType );
        ReadInt( value, "bladeTypeId", temp.bladeTypeId );
        ReadInt( value, "createdAt", temp.createdAt );
        ReadInt( value, "createdBy", temp.createdBy );
        ReadInt( value, "cutPressure", temp.cutPressure );
        ReadInt( value, "deletedAt", temp.deletedAt );
        ReadInt( value, "densityId", temp.densityId );
        ReadInt( value, "deviceModeId", temp.deviceModeId );
        ReadInt( value, "id", temp.id );
        ReadInt( value, "laserPowerId", temp.laserPowerId );
        ReadInt( value, "materialId", temp.materialId );
        ReadInt( value, "power", temp.power );
        ReadInt( value, "processingTypeId", temp.processingTypeId );
        ReadInt( value, "repeat", temp.repeat );
        ReadInt( value, "speed", temp.speed );
        ReadInt( value, "updatedAt", temp.updatedAt );
        ReadInt( value, "updatedBy", temp.updatedBy );
        
        ReadCodeName( value, "bitmapMode", temp.bitmapMode );
        ReadCodeName( value, "density", temp.density );
        ReadCodeName( value, "deviceMode", temp.deviceMode );
        ReadCodeName( value, "laserPower", temp.laserPower );
        ReadCodeName( value, "processingType", temp.processingType );

        paras.push_back( temp );
        }
}

/******************************************************************************/

// array of structs
void ReadMaterialList( json &object, materialList &mats )
{
    // aray, so keys are just numbers
    for ( auto& [key, value] : object.items())
        {
        VerifyMaterial( value );
        
        material_struct temp;

        ReadInt( value, "categoryID", temp.categoryId );
        ReadString( value, "categoryName", temp.categoryName );
        ReadInt( value, "createdAt", temp.createdAt );
        ReadInt( value, "createdBy", temp.createdBy );
        ReadInt( value, "deletedAt", temp.deletedAt );
        ReadDouble( value, "diameter", temp.diameter );
        ReadString( value, "helpUrl", temp.helpUrl );
        ReadInt( value, "id", temp.id );
        ReadString( value, "image", temp.image );
        ReadBool( value, "isPreset", temp.isPreset );
        ReadString( value, "saleStatus", temp.saleStatus );
        ReadString( value, "saleUrl", temp.saleUrl );
        ReadString( value, "status", temp.status );
        ReadDouble( value, "thickness", temp.thickness );
        ReadInt( value, "typeId", temp.typeId );
        ReadString( value, "typeName", temp.typeName );
        ReadInt( value, "updatedAt", temp.updatedAt );
        ReadInt( value, "updatedBy", temp.updatedBy );
        
        auto names = value.find("name");
        if (names != value.end())
            ReadLocalizedText( names.value(), temp.name );
        
        auto skus = value.find("skus");
        if (skus != value.end())
            ReadSkuList( skus.value(), temp.skus );
        
        auto paras = value.find("machineParas");
        if (paras != value.end())
            ReadMachineParas( paras.value(), temp.machineParas );
        
        mats.push_back( temp );
        }
}

/******************************************************************************/

void ReadDevices( json &device_data, XCS_device &dev )
{
    VerifyDevices( device_data );
    
    // data -- map of canvases with object cut parameters
    auto cut_data = device_data.find("data");
    if (cut_data != device_data.end())
        {
        keyList requiredCutDataKeys = { "dataType", "value" };
        keyList optionalCutDataKeys = { };
        ValidateKeys( cut_data.value(), requiredCutDataKeys, optionalCutDataKeys, "cut data" );
        
        // dataType == Map  -- this is just stupid, since the JSON basic type IS a map
        auto dataType = cut_data.value().find("dataType");
        assert( dataType.value() == string("Map") );
        
        // value == array of item cut parameters
        auto cut_items = cut_data.value().find("value");
        if (cut_items != cut_data.value().end())
            ReadCuts( cut_items.value(), dev.cut_data );
        }

    // power, number -- usually zero
    ReadInt( device_data, "power", dev.power );

    // optional, usually empty, but used for company supplied projects with links to material store
    auto materialList = device_data.find("materialList");
    if (materialList != device_data.end())
        ReadMaterialList( materialList.value(), dev.materialList );

    //  materialTypeList - always empty, no idea of contents
    auto materialTypeList = device_data.find("materialTypeList");
    if (materialTypeList != device_data.end() && materialTypeList.value().size() > 0)
        cout << "materialTypeList defined : " << materialTypeList.value().dump(4) << '\n';

    // material ID, string -- "MDP" == "user-defined material"
    ReadString( device_data, "id", dev.id );
}

/******************************************************************************/

void ReadTypeStyle( json &object, text_style_struct &style )
{
    VerifyTextStyle( object );
    
    ReadString( object, "align", style.align );
    ReadString( object, "fontFamily", style.fontFamily );
    ReadDouble( object, "fontSize", style.fontSize );
    ReadString( object, "fontSubfamily", style.fontSubfamily );
    ReadDouble( object, "leading", style.leading );
    ReadDouble( object, "letterSpacing", style.letterSpacing );
    ReadString( object, "fontSource", style.fontSource );
    ReadDouble( object, "curveX", style.curveX );
    ReadDouble( object, "curveY", style.curveY );
}

/******************************************************************************/

// forward definition, because someone got recursive with character glyphs
void ReadObject( json &object, object_data &obj );

void ReadCharJSON( json &object, charObjectArray &list)
{
    size_t count = object.size();
    list.reserve(count);

    // array of object cut settings
    
    for ( auto& [key, value] : object.items())
        {
        object_data temp;
        ReadObject(value, temp);
        list.push_back(temp);
        }
}

/******************************************************************************/

void ReadFontInfo( json &object, font_info &data)
{
    VerifyFontInfo( object );

    ReadDouble( object, "lineHeight", data.lineHeight );
    ReadDouble( object, "unitsPerEm", data.unitsPerEm );
    
    ReadDouble( object, "ascent", data.ascent );            // 1.4.11
    ReadDouble( object, "capHeight", data.capHeight );      // 1.4.11
    ReadDouble( object, "descent", data.descent );          // 1.4.11
    ReadDouble( object, "lineGap", data.lineGap );          // 1.4.11
    ReadDouble( object, "xHeight", data.xHeight );          // 1.4.11
}

/******************************************************************************/

void ReadGlyphData( json &object, glyph_data &data)
{
    VerifyGlyphData( object );
    
    ReadDouble( object, "advanceHeight", data.advanceHeight );
    ReadDouble( object, "advanceWidth", data.advanceWidth );
    ReadString( object, "dPath", data.dPath );
    
    ReadDouble( object, "leftBearing", data.leftBearing );  // 1.4.11
    ReadDouble( object, "topBearing", data.topBearing );    // 1.4.11
    
    auto bboxFound = object.find("bbox");               // 1.4.11
    if (bboxFound != object.end())
        {
        ReadDouble( object, "minY", data.bbox.minY );
        ReadDouble( object, "maxY", data.bbox.maxY );
        ReadDouble( object, "minX", data.bbox.minX );
        ReadDouble( object, "maxX", data.bbox.maxX );
        }
}

/******************************************************************************/

void ReadGlyphDataList( json &object, glyphDataMap &data)
{
    for ( auto& [key, value] : object.items())
        {
        glyph_data temp;
        temp.key = key;
        ReadGlyphData( value, temp );
        data[ temp.key ] =  temp;
        }
}

/******************************************************************************/

void ReadFontData( json &object, font_data &data)
{
    VerifyFontData( object );
    
    data.valid_ = true;
    
    auto font_info_iter = object.find("fontInfo");
    if (font_info_iter != object.end())
        ReadFontInfo( font_info_iter.value(), data.fontInfo );
    
    auto glyph_iter = object.find("glyphData");
    if (glyph_iter != object.end())
        ReadGlyphDataList( glyph_iter.value(), data.glyphData );
}

/******************************************************************************/

void ReadObject( json &object, object_data &obj )
{
    VerifyObject( object );
    
    ReadGUID( object, "id", obj.id );
    
    ReadDouble( object, "angle", obj.angle );
    ReadDouble( object, "height", obj.height );
    ReadDouble( object, "width", obj.width );
    ReadDouble( object, "x", obj.x );
    ReadDouble( object, "y", obj.y );
    ReadBool( object, "isClosePath", obj.isClosePath );
    ReadBool( object, "isFill", obj.isFill );
    ReadBool( object, "lockRatio", obj.lockRatio );
    ReadDouble( object, "offsetX", obj.offsetX );
    ReadDouble( object, "offsetY", obj.offsetY );
    ReadDouble( object, "graphicX", obj.graphicX );
    ReadDouble( object, "graphicY", obj.graphicY );
    ReadString( object, "dPath", obj.dPath );
    ReadDouble( object, "resolution", obj.resolution );
    ReadString( object, "text", obj.text );
    
    // empty so far, no idea what it is supposed to contain
    // so print it out if it contains anything
    auto dataFound = object.find("controlPoints");
    if (dataFound != object.end())
        {
        if (dataFound.value().size() > 0)
            {
            cout << dataFound.key() << " : " << dataFound.value() << '\n';
            }
        }
    
    ReadInt( object, "fillColor", obj.fillColor );
    ReadInt( object, "lineColor", obj.lineColor );
    
    ReadString( object, "type", obj.type );
    
    // DANGER - sometimes list of point2D, sometimes list of doubles, depeneds on type
    if (obj.type == "POLYGON")
        ReadDoubleList( object, "points", obj.polyPoints );
    else
        ReadPointList( object, "points", obj.points );
    
    ReadPoint2D( object, "localSkew", obj.localSkew );
    ReadPoint2D( object, "pivot", obj.pivot );
    ReadPoint2D( object, "scale", obj.scale );
    ReadPoint2D( object, "skew", obj.skew );
    ReadPoint2D( object, "endPoint", obj.endPoint );
    
    ReadInt( object, "zOrder", obj.zOrder );
    
    ReadGUID( object, "groupTag", obj.groupTag );
    ReadGUID( object, "sourceId", obj.sourceId );
    
    ReadIntList( object, "grayValue", obj.grayValue );
    ReadInt( object, "sharpness", obj.sharpness );
    ReadString( object, "base64", obj.base64data );
    ReadStringList( object, "filterList", obj.filterList );
    ReadBool( object, "colorInverted", obj.colorInverted );
    
    auto style = object.find("style");
    if (style != object.end())
        ReadTypeStyle( style.value(), obj.style );
    
    auto charJSON_data = object.find("charJSONs");
    if (charJSON_data != object.end())
        ReadCharJSON( charJSON_data.value(), obj.charJSONs );

    auto font_data = object.find("fontData");
    if (font_data != object.end())
        ReadFontData( font_data.value(), obj.fontData );

    // "layerColor" string hex RGB value "#2366ff"
    ReadString( object, "layerColor", obj.layerColor );
    
    // "layerTag" string hex RGB value "#2366ff"
    ReadString( object, "layerTag", obj.layerTag );
    
    // "fillRule" string "nonzero"  -- path only?
    ReadString( object, "fillRule", obj.fillRule );
    
    ReadString( object, "originColor", obj.originColor );
    ReadDouble( object, "radius", obj.radius );
    ReadDouble( object, "maxRadius", obj.maxRadius );
}

/******************************************************************************/

void ReadLayerData( json &object, layer_data &layer )
{
    VerifyLayerData( object );
    
    // id == key
    ReadString( object, "name", layer.name );
    ReadInt( object, "order", layer.order );
}

/******************************************************************************/

template<typename T>
void ReadCanvases( T input, canvasList &canvas_list )
{
    // this is an array, so keys are just numbers
    // this is stupid since each has a GUID key inside!
    for ( auto& [key, value] : input)
        {
        VerifyCanvas( value );
        
        canvas_struct temp;
        
        // "displays" value is [] of object definitions, with each object having a GUID inside
        auto objects = value.find("displays");
        if (objects != value.end())
            {
            //assert( objects.value().items().size() > 0 );
            for ( auto& [key4, value4] : objects.value().items())
                {
                object_data obj;
                ReadObject( value4, obj );
                temp.objects.insert( { obj.id, obj } );
                }
            }
        
        ReadGUID( value, "id", temp.id );
        ReadString( value, "title", temp.title );

        // layerData value is a map of layer_data, with a FFFS hex color string as key
        auto layerData = value.find("layerData");
        if (layerData != value.end())
            {
            for ( auto& [key5, value5] : layerData.value().items())
                {
                layer_data layer;
                ReadLayerData( value5, layer );
                layer.id = key5;
                temp.layerData.insert( { layer.id, layer } );
                }
            }
        
        canvas_list.push_back( temp );
        }
}

/******************************************************************************/

void ReadMeta( json &input, XCS_meta &meta )
{
    VerifyMeta( input );

    ReadInt( input, "date", meta.date );
    ReadString( input, "ua", meta.ua );
    ReadString( input, "version", meta.version );
}

/******************************************************************************/

// WHY is this a list/array? Do they plan to store the browser info of every editor?
void ReadMetaList( json &input, metaList &list )
{
    for ( auto& [key, value] : input.items())
        {
        XCS_meta temp;
        ReadMeta(value, temp);
        list.push_back(temp);
        }
}

/******************************************************************************/

void ReadXCSFile( std::istream &in, XCSDocument &doc )
{
    json root_data = json::parse(in);

#if 0
    // make sure the parser outputs what it read in without errors
    {
    std::ofstream debug( "debugJSON.xcs");
    debug << root_data;
    debug.close();
    }
#endif

#if 0
    // dump it all for debugging
    cout <<  root_data.dump(4) << '\n';
#endif

    VerifyXCSFile( root_data );

    // canvas - vector of object and size data, with a GUID id (corresponds to device and current CanvasID)
    auto canvas_iter = root_data.find("canvas");
    if (canvas_iter != root_data.end())
        ReadCanvases( canvas_iter.value().items(), doc.canvas );
    
    // device -- not always present!
    // defines cut/engrave parameters, plus several odd items  [ data, id, materialList, power ]
    auto device_iter = root_data.find("device");
    if (device_iter != root_data.end())
        ReadDevices( device_iter.value(), doc.device );
    
    ReadGUID( root_data, "canvasId", doc.canvasId );
    ReadString( root_data, "extId", doc.extId );
    ReadString( root_data, "version", doc.version );
    
    ReadInt( root_data, "created", doc.created );
    ReadInt( root_data, "modify", doc.modify );
    
    auto meta_iter = root_data.find("meta");
    if (meta_iter != root_data.end())
        ReadMetaList( meta_iter.value(), doc.meta );
}

/******************************************************************************/
/******************************************************************************/

json WriteJSONForPoint2D( const point2D &pt )
{
    json out;
    out[ "x" ] = pt.x;
    out[ "y" ] = pt.y;
    return out;
}

/******************************************************************************/

json WriteJSONforPoint2DList( const vector<point2D> &points )
{
    json out_list;
    
    for ( auto& pt : points)
        {
        json this_point = WriteJSONForPoint2D( pt );
        out_list.push_back( this_point );
        }
    
    return out_list;
}

/******************************************************************************/

json WriteJSONforDoubleList( const vector<double> &points )
{
    json out_list = points;
    
    return out_list;
}

/******************************************************************************/

json WriteJSONForTextStyle( const text_style_struct &style )
{
    json out;
    
    out[ "align" ] = style.align;
    out[ "fontFamily" ] = style.fontFamily;
    out[ "fontSize" ] = style.fontSize;
    out[ "fontSubfamily" ] = style.fontSubfamily;
    out[ "leading" ] = style.leading;
    out[ "letterSpacing" ] = style.letterSpacing;
    
    if (style.fontSource != string())
        out[ "fontSource" ] = style.fontSource;
    
    out[ "curveX" ] = style.curveX;
    out[ "curveY" ] = style.curveY;
    
    VerifyTextStyle( out );
    
    return out;
}

/******************************************************************************/

json WriteFontInfo( const font_info &data )
{
    json out;
    out[ "lineHeight" ] = data.lineHeight;
    out[ "unitsPerEm" ] = data.unitsPerEm;
    
    out[ "ascent" ] = data.ascent;       // 1.4.11
    out[ "capHeight" ] = data.capHeight; // 1.4.11
    out[ "descent" ] = data.descent;     // 1.4.11
    out[ "lineGap" ] = data.lineGap;     // 1.4.11
    out[ "xHeight" ] = data.xHeight;     // 1.4.11

    VerifyFontInfo( out );
    
    return out;
}

/******************************************************************************/

void to_json( json &j, const rect2D &g )
{
    j = json{ {"minX", g.minX}, {"minY", g.minY}, {"maxX", g.maxX}, { "maxY", g.maxY} };
}

json WriteGlyphData( const glyph_data &data )
{
    json out;
    
    out[ "advanceHeight" ] = data.advanceHeight;
    out[ "advanceWidth" ] = data.advanceWidth;
    out[ "dPath" ] = data.dPath;
    
    out [ "leftBearing" ] = data.leftBearing;   // 1.4.11
    out [ "topBearing" ] = data.topBearing;     // 1.4.11
    
    if ( !data.bbox.isNull() )                  // 1.4.11
        {
        out[ "bbox" ] = data.bbox;
        }
    
    VerifyGlyphData( out );
    
    return out;
}

/******************************************************************************/

json WriteGlyphDataList( const glyphDataMap &list )
{
    json out;
    
    for ( auto& [key, value] : list)
        {
        out[ key ] = WriteGlyphData(value);
        }
    
    return out;
}

/******************************************************************************/

json WriteFontData( const font_data &data )
{
    json out;
    out[ "fontInfo" ] = WriteFontInfo( data.fontInfo );
    out[ "glyphData" ] = WriteGlyphDataList( data.glyphData );
    
    VerifyFontData( out );
    return out;
}

/******************************************************************************/

// forward definition, because someone got recursive with character glyphs
json WriteObject( const object_data &value );

json WriteCharJSONs( const charObjectArray &list )
{
    json out;
    
    for ( auto &obj : list)
        {
        json temp = WriteObject( obj );
        out.push_back( temp );
        }
    
    return out;
}

/******************************************************************************/

json WriteObject( const object_data &value )
{
    json one_object;
    
    // required values
    one_object[ "id" ] = value.id;
    one_object[ "angle" ] = value.angle;
    one_object[ "width" ] = value.width;
    one_object[ "height" ] = value.height;
    one_object[ "x" ] = value.x;
    one_object[ "y" ] = value.y;
    one_object[ "isClosePath" ] = value.isClosePath;
    one_object[ "isFill" ] = value.isFill;
    one_object[ "lockRatio" ] = value.lockRatio;
    one_object[ "offsetX" ] = value.offsetX;
    one_object[ "offsetY" ] = value.offsetY;
    one_object[ "zOrder" ] = value.zOrder;
    one_object[ "localSkew" ] = WriteJSONForPoint2D( value.localSkew );
    one_object[ "pivot" ] = WriteJSONForPoint2D( value.pivot );
    one_object[ "scale" ] = WriteJSONForPoint2D( value.scale );
    one_object[ "skew" ] = WriteJSONForPoint2D( value.skew );
    one_object[ "type" ] = value.type;
    
    
    // optional values based on other factors
    // TODO - this won't work if we use real GUID code, need additional info, or init to a testable fake GUID
    if (value.groupTag != string())
        one_object[ "groupTag" ] = value.groupTag;
    if (value.sourceId != string())
        one_object[ "sourceId" ] = value.sourceId;
    
    
    // optional values based on object type
    bool basicShape = ( value.type == "CIRCLE" || value.type == "RECT"  || value.type == "LINE"
                        || value.type == "PEN" || value.type == "PATH" || value.type == "POLYGON" );
    if (basicShape)
        {
        one_object[ "fillColor" ] = value.fillColor;
        one_object[ "lineColor" ] = value.lineColor;
        }
    
    if (value.type == "LINE")
        {
        one_object[ "endPoint" ] = WriteJSONForPoint2D( value.endPoint );
        }
    
    if (value.type == "PATH")
        {
        one_object[ "graphicX" ] = value.graphicX;
        one_object[ "graphicY" ] = value.graphicY;
        one_object[ "dPath" ] = value.dPath;
        one_object[ "points" ] = json::array();    // empty vector
    
        if (value.fillRule != string())
            one_object[ "fillRule" ] = value.fillRule;
        }
    
    if (value.type == "PEN")
        {
        one_object[ "points" ] = WriteJSONforPoint2DList( value.points );
        one_object[ "controlPoints" ] = json::object(); // empty object
        }
    
    if (value.type == "POLYGON")
        {
        one_object[ "points" ] = WriteJSONforDoubleList( value.polyPoints );
        }
    
    if (value.type == "TEXT")
        {
        one_object[ "resolution" ] = value.resolution;
        one_object[ "text" ] = value.text;
        one_object[ "style" ] = WriteJSONForTextStyle( value.style );
        
        if (value.fontData.valid_)
            one_object[ "fontData" ] = WriteFontData( value.fontData );
        
        if (value.charJSONs.size() > 0)
            one_object[ "charJSONs" ] = WriteCharJSONs( value.charJSONs );
        }
    
    if (value.type == "BITMAP")
        {
        one_object[ "grayValue" ] = value.grayValue;    // vector of int, simple type works
        one_object[ "sharpness" ] = value.sharpness;
        one_object[ "filterList" ] = value.filterList;  // vector of string, simple type works
        one_object[ "base64" ] = value.base64data;
        one_object[ "colorInverted" ] = value.colorInverted;
        }

    // "layerColor" string hex RGB value "#2366ff"
    if (value.layerColor != string())
        one_object[ "layerColor" ] = value.layerColor;
    
    // "layerTag" string hex RGB value "#2366ff"
    if (value.layerTag != string())
        one_object[ "layerTag" ] = value.layerTag;
    
   if (value.originColor != string() )
        one_object[ "originColor" ] = value.originColor;    // 1.4.11
    
    if (value.type == "RECT")
        {
        one_object[ "radius" ] = value.radius;        // 1.4.11
        one_object[ "maxRadius" ] = value.maxRadius;  // 1.4.11
        }

    VerifyObject( one_object );
        
    return one_object;
}

/******************************************************************************/

json WriteJSONForObjects( const objectList &obj_list )
{
    json list_out;
    
    // stored as an array of structs, with internal GUID id
    for ( auto& [key, value] : obj_list)
        {
        assert( key == value.id );
        json one_object = WriteObject( value );
        list_out.push_back( one_object );
        }
    
    return list_out;
}

/******************************************************************************/

json WriteLayerData( const layer_data &layer )
{
    json one_layer;
    
    one_layer[ "name" ] = layer.name;
    one_layer[ "order" ] = layer.order;
    
    VerifyLayerData( one_layer );
    
    return one_layer;
}

/******************************************************************************/

json WriteJSONForLayerData( const layerDataList &layer_data )
{
    json list_out;
    
    // stored as a map of layerData with a FFFS hex color as key
    for ( auto& [key, value] : layer_data)
        {
        assert( key == value.id );
        list_out[ key ] = WriteLayerData( value );
        }
    
    return list_out;
}

/******************************************************************************/

json WriteJSONForCanvases( const canvasList &list )
{
    json canvas_list;

    // stored as an array of structs, with internal GUID id
    for ( auto& value: list)
        {
        json this_canvas;
        
        this_canvas[ "id" ] = value.id;
        this_canvas[ "title" ] = value.title;
        this_canvas[ "displays" ] = WriteJSONForObjects( value.objects );

        //  layerData, don't write unless it is defined
        if ( value.layerData.size() > 0 )
            this_canvas[ "layerData" ] = WriteJSONForLayerData( value.layerData );

        VerifyCanvas( this_canvas );
        canvas_list.push_back( this_canvas );
        }

    return canvas_list;
}

/******************************************************************************/

json WriteJSONForSkus( const sku_list &list )
{
    json out_list;
    
    for ( auto &item: list )
        {
        json out;
        
        out[ "code" ] = item.code;
        out[ "id" ] = item.id;
        out[ "key" ] = item.key;
        out[ "name" ] = item.name;
        
        VerifySku(out);
        out_list.push_back(out);
        }
    
    return out_list;
}

/******************************************************************************/

json WriteJSONForLocalizedText( const localized_list &names )
{
    json out_map;
    
    for ( auto& [key2, value2] : names)
        {
        out_map[ key2 ] = value2.text;
        }
    
    return out_map;
}

/******************************************************************************/

json WriteJSONForCodeName( const code_name_struct &item )           // KidsNextDoor
{
    json out;
    
    out[ "code" ] = item.code;
    out[ "id" ] = item.id;
    out[ "name" ] = item.name;
    
    VerifyCodeName(out);
    return out;
}

/******************************************************************************/

json WriteJSONForMachineParas( const machineParasList &list )
{
    json out_list;
    
    for ( auto &item: list )
        {
        json out;
        
        out[ "bitmapModeId" ] = item.bitmapModeId;
        out[ "bladeType" ] = item.bladeType;
        out[ "bladeTypeId" ] = item.bladeTypeId;
        out[ "createdAt" ] = item.createdAt;
        out[ "createdBy" ] = item.createdBy;
        out[ "cutPressure" ] = item.cutPressure;
        out[ "deletedAt" ] = item.deletedAt;
        out[ "densityId" ] = item.densityId;
        out[ "deviceModeId" ] = item.deviceModeId;
        out[ "id" ] = item.id;
        out[ "laserPowerId" ] = item.laserPowerId;
        out[ "materialId" ] = item.materialId;
        out[ "power" ] = item.power;
        out[ "processingTypeId" ] = item.processingTypeId;
        out[ "repeat" ] = item.repeat;
        out[ "speed" ] = item.speed;
        out[ "updatedAt" ] = item.updatedAt;
        out[ "updatedBy" ] = item.updatedBy;
        
        out[ "bitmapMode" ] = WriteJSONForCodeName( item.bitmapMode );
        out[ "density" ] = WriteJSONForCodeName( item.density );
        out[ "deviceMode" ] = WriteJSONForCodeName( item.deviceMode );
        out[ "laserPower" ] = WriteJSONForCodeName( item.laserPower );
        out[ "processingType" ] = WriteJSONForCodeName( item.processingType );

        VerifyMachineParas(out);
        out_list.push_back(out);
        }
    
    return out_list;
}

/******************************************************************************/

// empty object  json::object();
json WriteJSONForMaterialList( const materialList &list )
{
    json out_list;
    
    if (list.size() == 0)
        return json::array();    // empty vector
    
    // otherwise we have to build up the data structures
    for ( auto &item: list )
        {
        json temp;

        temp[ "categoryId" ] = item.categoryId;
        temp[ "categoryName" ] = item.categoryName;
        temp[ "createdAt" ] = item.createdAt;
        temp[ "createdBy" ] = item.createdBy;

        if (item.deletedAt < 0)
            temp[ "deletedAt" ] = nullptr;
        else
            temp[ "deletedAt" ] = item.deletedAt;

        temp[ "diameter" ] = item.diameter;
        temp[ "helpUrl" ] = item.helpUrl;
        temp[ "id" ] = item.id;
        temp[ "image" ] = item.image;
        temp[ "isPreset" ] = item.isPreset;
        temp[ "saleStatus" ] = item.saleStatus;
        temp[ "saleUrl" ] = item.saleUrl;
        temp[ "status" ] = item.status;
        temp[ "thickness" ] = item.thickness;
        temp[ "typeId" ] = item.typeId;
        temp[ "typeName" ] = item.typeName;
        temp[ "updatedAt" ] = item.updatedAt;
        temp[ "updatedBy" ] = item.updatedBy;
        temp[ "isPreset" ] = item.isPreset;
        temp[ "isPreset" ] = item.isPreset;

        temp[ "machineParas" ] = WriteJSONForMachineParas( item.machineParas );
        temp[ "name" ] = WriteJSONForLocalizedText( item.name );
        temp[ "skus" ] = WriteJSONForSkus( item.skus );
        
        VerifyMaterial( temp );
        
        out_list.push_back( temp );
        }
    
    return out_list;
}

/******************************************************************************/

json WriteJSONForLaserPlane( const laser_plane_struct &plane )
{
    json out_plane;
    
    out_plane[ "material" ] = plane.material;
    out_plane[ "thickness" ] = plane.thickness;
    
    if (plane.type == "LASER_CYLINDER")
        {
        out_plane[ "cushion" ] = plane.cushion;
        if (plane.diameter < 0)
            out_plane[ "diameter" ] = nullptr;
        else
            out_plane[ "diameter" ] = plane.diameter;
        if (plane.perimeter < 0)
            out_plane[ "perimeter" ] = nullptr;
        else
            out_plane[ "perimeter" ] = plane.perimeter;
        out_plane[ "focalLength" ] = plane.focalLength;
        out_plane[ "rotaryAttachmentType" ] = plane.rotaryAttachmentType;
        }
    
    if (plane.lightSourceMode != string())
        out_plane[ "lightSourceMode" ] = plane.lightSourceMode;
    
    VerifyLaserPlane( out_plane );
    
    json out;
    out[ plane.type ] = out_plane;
    return out;
}

/******************************************************************************/

json WriteJSONForCutParameters( const cut_parameter_struct &cut, const string &type )
{
    json item;
    
    item[ "repeat" ] = cut.repeat;
    item[ "speed" ] = cut.speed;
    
    if (type == "FILL_VECTOR_ENGRAVING" || type == "BITMAP_ENGRAVING")
        {
        item[ "bitmapMode" ] = cut.bitmapMode;
        item[ "density" ] = cut.density;
        }
    
    if (type == "KNIFE_CUTTING")
        item[ "cutPressure" ] = cut.cutPressure;
    else
        item[ "power" ] = cut.power;
    
    if (cut.materialType != string())
        item[ "materialType" ] = cut.materialType;
    
    if (cut.bitmapScanMode != string())
        item[ "bitmapScanMode" ] = cut.bitmapScanMode;
    
    if (cut.processingLightSource != string())
        item[ "processingLightSource" ] = cut.processingLightSource;
    
    if (cut.bitmapEngraveMode != string())
        item[ "bitmapEngraveMode" ] = cut.bitmapEngraveMode;
    
    // the breakPoint data is very poorly written, should have been an object, with default empty
    item[ "dotDuration" ] = cut.dotDuration;
    item[ "breakPointCount" ] = cut.breakPointCount;
    item[ "breakPointCut" ] = cut.breakPointCut;
    item[ "breakPointPower" ] = cut.breakPointPower;
    item[ "breakPointSize" ] = cut.breakPointSize;
    item[ "enableBreakPoint" ] = cut.enableBreakPoint;
    
    if (cut.dpi != 0)
        item[ "dpi" ] = cut.dpi;

    if (cut.powerMinMaxRange.size() > 0)
        item[ "powerMinMaxRange" ] = cut.powerMinMaxRange;
    
    VerifyCutParameters( item );
    
    json out;
    out[ cut.name ] = item;
    return out;
}

/******************************************************************************/

json WriteJSONForCutSettings( const object_cut_list &list )
{
    json out_list;
    
    for ( auto& [key, value] : list)
        {
        json item;
        
        assert( value.name == key );
        item[ "parameter" ] = WriteJSONForCutParameters( value.parameters, value.name );
        item[ "materialType" ] = value.materialType;
        item[ "processIgnore" ] = value.processIgnore;
        
        VerifyObjectCut( item );
        out_list[ key ] = item;
        }
    
    return out_list;
}

/******************************************************************************/

json WriteJSONForDisplays( const displayList &list )
{
    // another stupid "map in a vector"
    json out_list;
    
    for ( auto& [key, value] : list)
        {
        json item;
        
        item[ "data" ] = WriteJSONForCutSettings( value.cut_settings );
        item[ "isFill" ] = value.isFill;
        item[ "processingType" ] = value.processingType;
        item[ "type" ] = value.type;
        item[ "processIgnore" ] = value.processIgnore;
        
        VerifyDisplays( item );
        
        json wrapped;
        wrapped.push_back( value.id );
        wrapped.push_back( item );
        out_list.push_back( wrapped );
        }
    
    json out_map;
    out_map[ "dataType" ] = "Map";      // why store a map in a map with an array?  Just wastes time and space.
    out_map[ "value" ] = out_list;
    return out_map;
}

/******************************************************************************/

json WriteJSONForCutData( const cutList &list )
{
    // another stupid "map in a vector"
    json out_list;
    
    for ( auto& [key, value] : list)
        {
        json item;
        
        item[ "data" ] = WriteJSONForLaserPlane( value.laser_plane );
        item[ "displays" ] = WriteJSONForDisplays( value.displays );
    
        item[ "mode" ] = value.mode;
        item[ "needUpdateCanvas" ] = value.needUpdateCanvas;
    
        VerifyCanvasCut( item );
        
        json wrapped;
        wrapped.push_back( value.id );
        wrapped.push_back( item );
        out_list.push_back( wrapped );
        }
    
    json out_map;
    out_map[ "dataType" ] = "Map";      // why store a map in a map with an array?  Just wastes time and space.
    out_map[ "value" ] = out_list;
    return out_map;
}

/******************************************************************************/

json WriteJSONForDevices( const XCS_device &dev )
{
    json device;
    
    if (dev.power == -1)
        return json::value_t::null;      // still null
    
    device[ "data" ] = WriteJSONForCutData( dev.cut_data );
    device[ "materialList" ] = WriteJSONForMaterialList( dev.materialList );
    device[ "power" ] = dev.power;
    device[ "id" ] = dev.id;

    // WAITING - materialTypeList - currently always empty

    VerifyDevices( device );

    return device;
}

/******************************************************************************/

void to_json( json &j, const XCS_meta &g )
{
    j = json{ {"date", g.date}, {"ua", g.ua}, {"version", g.version} };

    VerifyMeta( j );
}

/******************************************************************************/

void WriteXCSFile( std::ostream &out, XCSDocument &doc )
{
    json root_data;
/*
The mis-matched array storage (that should have been maps) won't be easy to do in the to_json style functions
Some structs are easy, and some are a PITA because of the misguided storage.
Optional values could be handled in custom conversion routines and still use to_json
    the macros generally won't work

 */
    root_data[ "canvasId" ] = doc.canvasId;
    root_data[ "canvas" ] = WriteJSONForCanvases( doc.canvas );     // more or less all art objects and canvases

    // the device struct is not always present in files
    auto device = WriteJSONForDevices( doc.device );                // more or less the cut settings
    if (device != json::value_t::null)
        root_data[ "device" ] = device;

    root_data[ "extId" ] = doc.extId;
    root_data[ "version" ] = doc.version;
    
    if (doc.created != -1)
        root_data[ "created" ] = doc.created;
    if (doc.modify != -1)
        root_data[ "modify" ] = doc.modify;
    
    if (doc.meta.size() > 0)
        root_data[ "meta" ] = doc.meta;

    VerifyXCSFile( root_data );

    out << root_data;
}

/******************************************************************************/
/******************************************************************************/

// Why was this stored as 2 lists instead of a single list of object&cut_settings?  Probably legacy code.

// Groups do not have their own cut settings, just exist in "groupTag" per object.
// Subgroups are not kept in the software, there is only one level of grouping (ouch).

/// Make sure all the GUIDS and labels match up.
/// strict is for files we generate. !strict is for files from XCS, because they have bugs.
/// Use cleanUpMesses to fix files from XCS with bugs
void MatchUpIDs( XCSDocument &doc, const string &filename, bool strict, bool cleanUpMesses )
{
    std::unordered_set<GUID> canvases;
    std::unordered_map<GUID,GUID> objects;      // obj_key, canvas_key
    std::unordered_set<GUID> canvases_to_cleanup;
    std::unordered_map<GUID,GUID> objects_to_cleanup;   // obj_key, canvas_key


    // build list of canvas GUIDS
    for ( auto &item : doc.canvas )
        {
        assert( canvases.count( item.id ) == 0 );   // should not be duplicates
        canvases.insert( item.id );
        
        // build list of object GUIDS from each canvas
        for ( auto& [key, value] : item.objects )
            {
            assert( key == value.id );
            assert( objects.count( key ) == 0 );   // should not be duplicates
            objects.insert( { key, item.id } );
            }
        }

#if 0
// debug
    cout << "filename" << " canvases (" << doc.canvas.size() << "):\n";
    for ( auto &item : canvases)
        {
        cout << "\t" << item << '\n';
        }
    
    cout << filename << " objects (" << objects.size() << "):\n";
    for ( auto & [key, value] : objects)
        {
        cout << "\t" << key << " in canvas " << value << '\n';
        }
#endif

    // match document.canvasID to one of the canvases
    if ( canvases.count( doc.canvasId ) == 0 )
        {
        cout << "WARNING - active canvas " << doc.canvasId << " is not defined in canvas list of " << filename << "\n";
        }

    // match all canvases and objects
    for ( auto& [key, value] : doc.device.cut_data )
        {
        // verify that this canvas exists in both lists
        assert( key == value.id );
        if (canvases.count( key ) == 0)
            {
            if (strict)
                {
                cout << "WARNING: canvas " << key << " is used in cut list, but not defined in canvas list of " << filename << "\n";
                }
            if (cleanUpMesses)
                {
                // can't modify the list while iterating - so we'll save the item for deletion
                canvases_to_cleanup.insert( key );
                }
            }
        else
            canvases.erase( key );
        
        // verify that this object exists in both lists
        for ( auto& [key2, value2] : value.displays )
            {
            assert( key2 == value2.id );
            
            // validate cut_settings
            if (!isValidCutType(value2.processingType))
                {
                cout << "WARNING: object " << key2 << " has unknown cut type " << value2.processingType << "\n";
                }
            
            // ignore the types not used, they may not always be defined
            if (value2.cut_settings.count(value2.processingType) == 0)
                {
                cout << "WARNING: object " << key2 << " missing cut data for " << value2.processingType << "\n";
                }

            // if object does not exist, or is on the wrong canvas  (duplicates sometimes occur on different canvases)
            if (objects.count( key2 ) == 0 || (objects[key2] != key))
                {
                if (strict)
                    {
                    cout << "WARNING: object " << key2 << " is used in cut list, but not defined in canvas objects list of " << filename << "\n";
                    }
                if (cleanUpMesses)
                    {
                    // can't modify the list while iterating - so we'll save the item for deletion
                    // pair is object key (unique), canvas key (not unique, holds several objects) - be careful here
                    objects_to_cleanup.insert( { key2, key } );
                    }
                }
            else
                objects.erase( key2 );
            
            }   // object cut settings
        }   // canvases


    // check for leftover canvases and objects
    for ( auto &item : canvases)
        {
        cout << "WARNING: canvas " << item << " is defined, but not found in cut list of " << filename << "\n";
        }
    
    for ( auto & [key, value] : objects)
        {
        cout << "WARNING:  object " << key << " in canvas " << value;
        cout << " is defined, but not found in cut list of " << filename << "\n";
        }
    
    
    // remove items marked for deletion
    if (cleanUpMesses)
        {
        // remove unused object definitions
        for ( const auto& [obj_key, canv_key]  : objects_to_cleanup )
            doc.device.cut_data[ canv_key ].displays.erase( obj_key );
        
        // double check for empty canvases after removing object definitions
        for ( auto& [key, value] : doc.device.cut_data )
            {
            assert( key == value.id );
            if (value.displays.size() == 0)
                // can't modify the list while iterating - so we'll save the item for deletion
                canvases_to_cleanup.insert( key );
            }   // canvases
        
        // remove unused canvases AFTER objects, because objects are likely to be in those canvases
        for ( auto &key : canvases_to_cleanup )
            doc.device.cut_data.erase( key );
        
        }   // cleanUpMesses


    // make sure a cut setting object exists for object process type
    for ( auto& [key, value] : doc.device.cut_data )
        {
        assert( key == value.id );
        
        // iterate all objects
        for ( auto& [key2, value2] : value.displays )
            {
            assert( key2 == value2.id );
            auto process = value2.processingType;
            if (value2.cut_settings.count(process) == 0)
                {
                cout << "WARNING - object " << key2 << " does not have cut settings for " << process << "\n";
                }
            }   // object cut settings
        } // canvases

}

/******************************************************************************/
/******************************************************************************/

/// copy all of doc1, resolve conflicting GUIDS and strings with doc2, then copy doc2 contents into merged canvases
void MergeXCSCanvases( std::ostream &out, const XCSDocument &doc1, XCSDocument &doc2 )
{
    XCSDocument mergedDoc = doc1;
    
    // find and resolve all conflicting GUIDS between doc1 and doc2
    std::unordered_map<GUID,GUID> remap_canvas_guids;      // old, new
    std::unordered_map<GUID,GUID> remap_object_guids;      // old, new
    std::unordered_map<GUID,GUID> remap_group_guids;       // old, new
    std::unordered_set<GUID> canvases;
    std::unordered_set<string> canvas_names;
    std::unordered_set<GUID> group_tags;
    std::unordered_map<GUID,GUID> objects;      // obj_key, canvas_key
    
    // doc1 - just insert into lists of seen GUIDs
    for ( auto &item : mergedDoc.canvas )
        {
        assert( canvases.count( item.id ) == 0 );   // should not be duplicates
        canvases.insert( item.id );
        
        assert( canvas_names.count( item.title ) == 0 );   // should not be duplicates
        canvas_names.insert( item.title );
        
        // build list of object GUIDS from each canvas
        for ( auto& [key, value] : item.objects )
            {
            assert( key == value.id );
            assert( objects.count( key ) == 0 );   // should not be duplicates
            objects.insert( { key, item.id } );
            
            if (value.groupTag != GUID())
                group_tags.insert( value.groupTag );
            }
        }
    
    // doc2 - check for duplicates and insert into list of seen GUIDs
    for ( auto &item : doc2.canvas )
        {
        auto old_id = item.id;
        auto new_id = item.id;
        while ( canvases.count( new_id ) != 0 )
            {
            GUID replacement = NewBarelyUniqueIDString();
            remap_canvas_guids[ old_id ] = replacement;
            new_id = replacement;
            }
        // now known to be unique
        item.id = new_id;
        canvases.insert( new_id );
        
        
        while ( canvas_names.count( item.title ) != 0 )
            {
            item.title += "_merge";
            }
        canvas_names.insert( item.title );
        
        
        // build list of object GUIDS from each canvas
        for ( auto& [key, value] : item.objects )
            {
            assert( key == value.id );
            auto old_key = key;
            auto new_key = key;
            while ( objects.count( new_key ) != 0 )
                {
                GUID replacement = NewBarelyUniqueIDString();
                remap_object_guids[ old_key ] = replacement;
                new_key = replacement;
                }
            
            if (value.groupTag != GUID() )
                {
                GUID oldtag = value.groupTag;
                GUID newtag = value.groupTag;
                if (remap_group_guids.count( oldtag ) != 0)
                    {
                    newtag = remap_group_guids[ oldtag ];
                    }
                else
                    {
                    while( group_tags.count( newtag ) != 0 )
                        {
                        GUID replacement = NewBarelyUniqueIDString();
                        remap_group_guids[ oldtag ] = replacement;
                        newtag = replacement;
                        }
                    }
                value.groupTag = newtag;
                }
            
            // we need to modify the objects map, but can't do that while itereating the objects map
            // so we'll do the remapping later

            // now known to be unique
            objects.insert( { new_key, item.id } );
            }
        }


    // remap GUIDS as needed to make sure they all match up
    //  groups should already be handled above

    // double check canvases, and remap canvas object IDs
    for ( auto &item : doc2.canvas )
        {
        if ( remap_canvas_guids.count(item.id) != 0)
            {
            item.id = remap_canvas_guids[ item.id ];        // this shouldn't happen, already be done above
            }
        
        // we can't change object map while iterating it, so we need to check for changed IDs
        for ( auto& [key, value] : remap_object_guids )
            {
            if ( item.objects.count( key ) != 0 )
                {
                // remove and replace under new key
                object_data obj = item.objects[ key ];
                obj.id = value;
                item.objects.erase( key );
                item.objects[ value ] = obj;
                }
            }
        }
    
    auto &cut_data2 = doc2.device.cut_data;
    
    // we can't change cut_data map while iterating it, so we need to check for changed IDs
    for ( auto& [key, value] : remap_canvas_guids )
        {
        if ( cut_data2.count( key ) != 0 )
            {
            // remove and replace under new key
            cut_struct cut = cut_data2[ key ];
            cut.id = value;
            cut_data2.erase( key );
            cut_data2[ value ] = cut;
            }
        }
    
    // now remap object data in the cut.displays list
    for ( auto& [key, value] : cut_data2 )
        {
        assert( key == value.id );
        
        // we can't change displays map while iterating it, so we need to check for changed IDs
        for ( auto& [key2, value2] : remap_object_guids )
            {
            if ( value.displays.count( key2 ) != 0 )
                {
                // remove and replace under new key
                displays_struct obj = value.displays[ key2 ];
                obj.id = value2;
                value.displays.erase( key2 );
                value.displays[ value2 ] = obj;
                }
            }
        }
    

    // copy doc2 canvases (includes objects), cuts, and materials into merged document
    mergedDoc.canvas.insert( mergedDoc.canvas.end(), doc2.canvas.cbegin(), doc2.canvas.cend() );
    
    mergedDoc.device.cut_data.insert( cut_data2.cbegin(), cut_data2.cend() );
    
    auto &material2 = doc2.device.materialList;
    mergedDoc.device.materialList.insert( mergedDoc.device.materialList.end(), material2.cbegin(), material2.cend() );
    
    
    // validate the merged document
    MatchUpIDs( mergedDoc, "merged_document", true, true );
    
    // write out the merged document
    WriteXCSFile( out, mergedDoc );
}

/******************************************************************************/
/******************************************************************************/

GUID XCSAddCanvas( XCSDocument &doc, string name )
{
    // fill in our structs, make sure IDs match
    canvas_struct new_canvas;
    new_canvas.id = NewBarelyUniqueIDString();
    new_canvas.title = name;
    // leave objects empty for now
    
    cut_struct new_cut_canvas;
    new_cut_canvas.id = new_canvas.id;
    // leave displays empty for now
    new_cut_canvas.mode = "LASER_PLANE";
    new_cut_canvas.needUpdateCanvas = false;
    new_cut_canvas.laser_plane.type = "LASER_PLANE";
    // leave the rest of the laser plane at default values
    
    
    // add to canvaslist
    doc.canvas.push_back(new_canvas);
    
    // add to cut list
    doc.device.power = 1;
    doc.device.cut_data[ new_canvas.id ] =  new_cut_canvas;
    
    // make this the current canvas
    doc.canvasId = new_canvas.id;
    
    return new_canvas.id;
}

/******************************************************************************/

void CreateDefaultCutSettings( object_cut_list &list )
{
    // FILL_VECTOR_ENGRAVING
    cut_types_struct fillVec;
    fillVec.name = "FILL_VECTOR_ENGRAVING";
    list[ fillVec.name ] = fillVec;

    // VECTOR_ENGRAVING
    cut_types_struct engrave;
    engrave.name = "VECTOR_ENGRAVING";
    list[ engrave.name ] = engrave;
    
    // VECTOR_CUTTING
    cut_types_struct cut;
    cut.name = "VECTOR_CUTTING";
    list[ cut.name ] = cut;
    
    // KNIFE_CUTTING
    cut_types_struct knife;
    knife.name = "KNIFE_CUTTING";
    list[ knife.name ] = knife;
}

/******************************************************************************/

// power is used as cutPressure for xcsKnifeCut
void ChangeObjectCutSettings( XCSDocument &doc, GUID &object, xcsCutType cut, int power, int repeat, int speed, string bitmapMode, int density, string materialType)
{
    string cut_type = cutTypeToString( cut );
    cut_parameter_struct new_cut_values( power, repeat, speed, bitmapMode, density, power );
    new_cut_values.materialType = materialType;     // not used often
    
    // if object GUID is empty, change all objects
    // else find specific object to change
    bool changeAll = (object == GUID());
    
    // iterate all canvases
    for ( auto& [key, value] : doc.device.cut_data )
        {
        assert( key == value.id );
        
        // iterate all objects
        for ( auto& [key2, value2] : value.displays )
            {
            assert( key2 == value2.id );

            if (changeAll || key2 == object)
                value2.cut_settings[ cut_type ].parameters = new_cut_values;
            
            }   // object cut settings
        } // canvases
}

/******************************************************************************/

void ChangeObjectCutMode( XCSDocument &doc, GUID &object, xcsCutType cut)
{
    string cut_type = cutTypeToString( cut );
    
    // if object GUID is empty, change all objects
    // else find specific object to change
    bool changeAll = (object == GUID());
    
    // iterate all canvases
    for ( auto& [key, value] : doc.device.cut_data )
        {
        assert( key == value.id );
        
        // iterate all objects
        for ( auto& [key2, value2] : value.displays )
            {
            assert( key2 == value2.id );

            if (changeAll || key2 == object)
                value2.processingType = cut_type;
            
            }   // object cut settings
        } // canvases
}

/******************************************************************************/

canvas_struct *FindCanvas( XCSDocument &doc, GUID canvas_id )
{
    // default to current canvas if no GUID provided
    if (canvas_id == GUID())
        canvas_id = doc.canvasId;
    
    canvas_struct *this_canvas = NULL;
    assert(doc.canvas.size()> 0);
    for ( auto &item : doc.canvas )
        {
        if (item.id == canvas_id)
            this_canvas = &item;
        }
    
    assert(this_canvas != NULL);
    return this_canvas;
}

/******************************************************************************/

object_data *FindObject( XCSDocument &doc, GUID &object )
{
    assert( object != GUID() );
    
    // iterate all canvases
    for ( auto &item : doc.canvas )
        {
        // iterate all objects
        for ( auto& [key, value] : item.objects )
            {
            assert( key == value.id );
            
            if (key == object)
                return &value;
            }
        }
    
    return NULL;
}

/******************************************************************************/

displays_struct *FindObjectCutSettings( XCSDocument &doc, GUID &object )
{
    assert( object != GUID() );
    
    // iterate all canvases
    for ( auto& [key, value] : doc.device.cut_data )
        {
        assert( key == value.id );
        
        // iterate all objects
        for ( auto& [key2, value2] : value.displays )
            {
            assert( key2 == value2.id );

            if (key2 == object)
                return &value2;
            
            }   // object cut settings
        } // canvases
    
    return NULL;
}

/******************************************************************************/

void FindDocumentBounds( XCSDocument &doc, point2D &topLeft, point2D &bottomRight)
{
    bool firstObject = true;
    
    // iterate each object on every canvas
    for ( auto& value: doc.canvas)
        {
        for ( auto& [key2, value2] : value.objects )
            {
            double xx = value2.x;
            double yy = value2.y;
            
            if (value2.type == "POLYGON")
                {
                xx += value2.offsetX;
                yy += value2.offsetY;
                }
                
            if (firstObject)
                {
                topLeft.x = xx;
                topLeft.y = yy;
                bottomRight.x = xx + value2.width;
                bottomRight.y = yy + value2.height;
                firstObject = false;
                }
            else
                {
                topLeft.x = std::min(topLeft.x, xx );
                topLeft.y = std::min(topLeft.y, yy );
                bottomRight.x = std::max(bottomRight.x, xx + value2.width );
                bottomRight.y = std::max(bottomRight.y, yy + value2.height );
                }
            }
        }
}

/******************************************************************************/

// TODO: share and reuse code for basic objects
GUID XCSAddObjectRect( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut, bool isFilled, GUID group )
{
    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "RECT";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.isFill = isFilled;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "RECT";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

GUID XCSAddObjectCircle( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut, bool isFilled, GUID group )
{
    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "CIRCLE";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.isFill = isFilled;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "CIRCLE";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

GUID XCSAddObjectLine( XCSDocument &doc, GUID &canvas_id, point2D topLeft, point2D size,
                    xcsCutType cut, GUID group )
{
    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "LINE";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.isFill = false;
    new_object.endPoint = size;
    new_object.isClosePath = false;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = false;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "LINE";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

GUID XCSAddObjectText( XCSDocument &doc, GUID &canvas_id, point2D topLeft, double size, string text,
                    xcsCutType cut, bool isFilled, GUID group )
{
    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "TEXT";
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.resolution = 1;
    new_object.isFill = isFilled;
    new_object.text = text;
//    new_object.pivot = point2D( size*point*8/2.0, size*point/2.0 ); // doesn't change where the text rotates
    
    // leave text style at mostly the default for now
    new_object.style.fontSize = size;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "TEXT";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

GUID XCSAddObjectPen( XCSDocument &doc, GUID &canvas_id, vector<point2D> &points,
                    bool closedPath, bool isFilled,
                    xcsCutType cut, GUID group )
{
    // find bounds of the points
    double top = points[0].y;
    double bottom = points[0].y;
    double left = points[0].x;
    double right = points[0].x;
    for (size_t i = 1; i < points.size(); ++i)
        {
        top = std::min( top, points[i].y );
        bottom = std::max( bottom, points[i].y );
        left = std::min( left, points[i].x );
        right = std::max( right, points[i].x );
        }
    point2D topLeft( left, top );
    assert( right >= left );
    assert( bottom >= top );
    point2D size( right - left, bottom - top );


    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "PEN";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.isFill = isFilled;
    new_object.endPoint = size;
    new_object.isClosePath = closedPath || isFilled;
    new_object.points = points;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "PEN";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

// REVISIT - always store as point2D to reduce confusion?
void ListPoint2DtoPolyPoints( const vector<point2D> &points, vector<double> &poly )
{
    poly.resize( points.size() * 2 );
    
    for (size_t i = 0; i < points.size(); ++i)
        {
        poly[2*i+0] = points[i].x;
        poly[2*i+1] = points[i].y;
        }
}

/******************************************************************************/

GUID XCSAddObjectPolygon( XCSDocument &doc, GUID &canvas_id, vector<point2D> &points,
                    bool isFilled, xcsCutType cut, GUID group )
{
    // find bounds of the points
    double top = points[0].y;
    double bottom = points[0].y;
    double left = points[0].x;
    double right = points[0].x;
    for (size_t i = 1; i < points.size(); ++i)
        {
        top = std::min( top, points[i].y );
        bottom = std::max( bottom, points[i].y );
        left = std::min( left, points[i].x );
        right = std::max( right, points[i].x );
        }
    point2D topLeft( left, top );
    assert( right >= left );
    assert( bottom >= top );
    point2D size( right - left, bottom - top );


    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "POLYGON";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.isFill = isFilled;
    new_object.endPoint = size;
    new_object.isClosePath = true;   // polygon is always closed
    ListPoint2DtoPolyPoints( points, new_object.polyPoints );
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "POLYGON";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

/******************************************************************************/

// size appears to be ignored, and redefined by the size of the path & scale
GUID XCSAddObjectSVGPath( XCSDocument &doc, GUID &canvas_id, string svg_path,
                    point2D topLeft, point2D size, bool isFilled,
                    xcsCutType cut, GUID group )
{
    // fill in our structs, make sure IDs match
    object_data new_object;
    new_object.id = NewBarelyUniqueIDString();
    new_object.type = "PATH";
    new_object.height = size.y;
    new_object.width = size.x;
    new_object.x = topLeft.x;
    new_object.y = topLeft.y;
    new_object.offsetX = topLeft.x;
    new_object.offsetY = topLeft.y;
    new_object.offsetY = topLeft.y;
    new_object.graphicX = topLeft.x;
    new_object.graphicY = topLeft.y;
    new_object.isFill = isFilled;
    new_object.endPoint = size;
    new_object.isClosePath = true;
    new_object.dPath = svg_path;
    
    if (group != GUID())
        new_object.groupTag = group;
    
    displays_struct new_display_struct;
    new_display_struct.id = new_object.id;
    new_display_struct.isFill = isFilled;
    
    string cut_type = cutTypeToString( cut );
    new_display_struct.processingType = cut_type;
    new_display_struct.type = "PATH";

    CreateDefaultCutSettings( new_display_struct.cut_settings );

    // find canvas based on GUID
    canvas_struct *this_canvas = FindCanvas( doc, canvas_id );
    
    // add object to canvas
    this_canvas->objects.insert( { new_object.id, new_object } );
    
    // add object to cut list
    doc.device.cut_data[ canvas_id ].displays[ new_object.id ] =  new_display_struct;
    
    return new_object.id;
}

// TODO: BITMAP -- a PNG image with base64 encoding
// input an image file?   position, width, height.


/******************************************************************************/

struct {
    bool operator()(object_data * a, object_data * b) const
        { if (a->y < b->y)  return true;
          if (a->y > b->y)  return false;
          // equal y case falls through to here
          return (a->x < b->x);
        }
} readingLess;

// which order is best? topleft increment or decrement?
// NOTE - increment didn't work, still random burn order
void SortObjectsZOrder( XCSDocument &doc, bool increasing )
{
    int zIncrement = increasing ? 1 : -1;
    vector <object_data *> sortingList;

    // iterate all canvases
    for ( auto &item : doc.canvas )
        {
        int  zCurrent = 0;
        
        sortingList.clear();
        sortingList.reserve( item.objects.size() );
        
        // iterate all objects into temporary list
        for ( auto& [key, value] : item.objects )
            {
            assert( key == value.id );
            sortingList.push_back( &value );
            }
        
        // sort by topLeft (reading order)
        sort( sortingList.begin(), sortingList.end(), readingLess );
        
        // write out zOrder by sorted list
        for (size_t i = 0; i < sortingList.size(); ++i)
            {
            sortingList[i]->zOrder = zCurrent;
            zCurrent += zIncrement;
            }
        }
}

/******************************************************************************/
/******************************************************************************/
