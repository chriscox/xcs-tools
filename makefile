#
#   XCS-Tools
#    Copyright 2022-2023 Chris Cox
#    Distributed under the MIT License 
#

#
# Macros
#

INCLUDE = -I.

CFLAGS = $(INCLUDE) -O3
CPPFLAGS = -std=c++20 $(INCLUDE) -O3

CLIBS = -lm
CPPLIBS = -lm

DEPENDENCYFLAG = -M



BINARIES = xcs_tools lqx_tools

all : $(BINARIES)

lqx_main.o: lqx_main.cpp json.hpp xcs_document.hpp

main.o: main.cpp json.hpp xcs_document.hpp Grids.hpp

SVG.o: SVG.cpp json.hpp xcs_document.hpp

Grids.o: Grids.cpp json.hpp xcs_document.hpp Grids.hpp

xcs_document.o: xcs_document.cpp xcs_document.hpp json.hpp

xcs_tools: main.o SVG.o Grids.o xcs_document.o
	$(CXX) $(CPPFLAGS) $^ -o $@

lqx_tools: lqx_main.o xcs_document.o
	$(CXX) $(CPPFLAGS) $^ -o $@


SUFFIXES:
.SUFFIXES: .c .cpp


# declare some targets to be fakes without real dependencies
.PHONY : clean dependencies

# remove all the stuff we build
clean : 
		rm -f *.o $(BINARIES)


# generate dependency listing from all the source files
# used for double checking problems with headers
# this does NOT go in the makefile

SOURCES = $(wildcard *.c)  $(wildcard *.cpp)
dependencies :   $(SOURCES)
	$(CXX) $(DEPENDENCYFLAG) $(CPPFLAGS) $^

